/**
 *
 */
package com.hubloy.sms.base.orm;

import java.util.Calendar;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import com.hubloy.sms.helpers.IDHelpers;



/**
 * Manage common entity fields
 * 
 * @since 1.0.0
 * 
 * @author Paul
 *
 * @param <E>
 */
public class EntityListener<E> {
	
	@PrePersist
	public void monitorPrePersist(AbstractEntity e) {
		e.setVersion(1);
		e.setItemId(IDHelpers.id());
		e.setDateCreated(Calendar.getInstance().getTime());
		e.setDeleted(false);
	}

	@PreUpdate
	public void monitorPreUpdate(AbstractEntity e) {
		e.setDateUpdated(Calendar.getInstance().getTime());
	}
}