/**
 *
 */
package com.hubloy.sms.base.orm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

import org.hibernate.annotations.GenericGenerator;

/**
 * Base Entity class Common entity functions
 * 
 * @since 1.0.0
 * 
 * @author Paul
 *
 */
@MappedSuperclass
@EntityListeners(EntityListener.class)
public class AbstractEntity implements Serializable {

	private static final long serialVersionUID = 785175327893141674L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	@Column(name = "id", nullable = false, unique = true, updatable = false)
	protected Long id;

	@Version
	private int version;

	@Column(name = "item_id", nullable = false, unique = true)
	protected String itemId;

	@Column(name = "date_created", updatable = false, columnDefinition = "timestamp default now()")
	private Date dateCreated;

	@Column(name = "date_updated", nullable = true)
	private Date dateUpdated;

	@Column(name = "deleted", nullable = false, columnDefinition = "boolean default false")
	private boolean deleted = false;

	public Long getId() {

		return id;
	}

	public void setId(Long id) {

		this.id = id;
	}

	public int getVersion() {

		return version;
	}

	public void setVersion(int version) {

		this.version = version;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public Date getDateCreated() {

		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {

		this.dateCreated = dateCreated;
	}

	public Date getDateUpdated() {

		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {

		this.dateUpdated = dateUpdated;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
}