package com.hubloy.sms.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * Base Helper
 * 
 * @author Paul Kevin
 *
 * @version enter version, 26 Feb 2018
 *
 * @since  jdk 1.8
 */
public class BaseHelper {

	private static Logger LOGGER = LoggerFactory.getLogger(BaseHelper.class);

	/**
	 * Get Logger
	 * 
	 * @return
	 */
	public static Logger log() {
		return LOGGER;
	}
}