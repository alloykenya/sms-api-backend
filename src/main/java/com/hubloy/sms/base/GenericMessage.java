package com.hubloy.sms.base;

import java.io.Serializable;
import java.util.Date;

import com.hubloy.sms.constants.ResponseStatus;

/**
 * 
 * GenericMessage
 * 
 * @author Paul Kevin
 *
 * @version enter version, 4 Apr 2018
 *
 * @since  jdk 1.8
 */
public class GenericMessage implements Serializable{

	private static final long serialVersionUID = -5780693404649328825L;

	private String status;

	private String message;

	private Date date;
	
	public GenericMessage(){
		super();
		this.date = new Date();
	}

	public GenericMessage(ResponseStatus responseStatus, String message) {
		super();
		this.status = responseStatus.name().toLowerCase();
		this.message = message;
		this.date = new Date();
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(ResponseStatus responseStatus) {
		this.status = responseStatus.name().toLowerCase();
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}