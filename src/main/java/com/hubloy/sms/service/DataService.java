package com.hubloy.sms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hubloy.sms.beans.common.DataBean;
import com.hubloy.sms.constants.MessageStatus;
import com.hubloy.sms.orm.account.Account;
import com.hubloy.sms.orm.account.AccountUser;
import com.hubloy.sms.orm.account.User;
import com.hubloy.sms.repository.account.AccountUserRepository;
import com.hubloy.sms.repository.contacts.ContactGroupRepository;
import com.hubloy.sms.repository.contacts.ContactRespository;
import com.hubloy.sms.repository.message.MessageToSendRepository;

@Service
public class DataService {

	private final AccountUserRepository accountUserRepository;

	private final MessageToSendRepository messageToSendRepository;

	private final ContactRespository contactRespository;

	private final ContactGroupRepository contactGroupRepository;

	@Autowired
	public DataService(AccountUserRepository accountUserRepository, MessageToSendRepository messageToSendRepository,
			ContactRespository contactRespository, ContactGroupRepository contactGroupRepository) {
		super();
		this.accountUserRepository = accountUserRepository;
		this.messageToSendRepository = messageToSendRepository;
		this.contactRespository = contactRespository;
		this.contactGroupRepository = contactGroupRepository;
	}

	public DataBean getData(User user) {
		DataBean bean = new DataBean();
		AccountUser accountUser = accountUserRepository.findByUser(user);
		if (accountUser != null) {
			Account account = accountUser.getAccount();
			bean.setContacts(contactRespository.countByAccount(account));
			bean.setGroups(contactGroupRepository.countByAccount(account));
			bean.setMessagesPending(
					messageToSendRepository.countByAccountAndMessageStatus(account, MessageStatus.Pending));
			bean.setMessagesSent(messageToSendRepository.countByAccountAndMessageStatus(account, MessageStatus.Sent));
		}
		return bean;
	}
}
