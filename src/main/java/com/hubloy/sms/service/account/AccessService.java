package com.hubloy.sms.service.account;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hubloy.sms.beans.account.AccountUserBean;
import com.hubloy.sms.beans.account.UserBean;
import com.hubloy.sms.constants.AccountStatus;
import com.hubloy.sms.constants.ResponseStatus;
import com.hubloy.sms.orm.account.Account;
import com.hubloy.sms.orm.account.AccountUser;
import com.hubloy.sms.orm.account.Profile;
import com.hubloy.sms.orm.account.User;
import com.hubloy.sms.repository.account.AccountUserRepository;
import com.hubloy.sms.repository.account.ProfileRepository;
import com.hubloy.sms.repository.account.UserRepository;

@Service
public class AccessService {

	private final ProfileRepository profileRepository;

	private final AccountUserRepository accountUserRepository;
	
	private final UserRepository userRepository;

	@Autowired
	public AccessService(ProfileRepository profileRepository, AccountUserRepository accountUserRepository,
			UserRepository userRepository) {
		super();
		this.profileRepository = profileRepository;
		this.accountUserRepository = accountUserRepository;
		this.userRepository = userRepository;
	}

	/**
	 * Get profile
	 * 
	 * @param user
	 * @return
	 */
	public UserBean getProfile(User user) {
		return userToBean(user);
	}

	/**
	 * Get Account user
	 * 
	 * @param user
	 * @return
	 */
	public AccountUserBean getUserAccount(User user) {
		AccountUser accountUser = accountUserRepository.findByUser(user);
		if (accountUser != null) {
			return accountUserToBean(accountUser);
		}
		return new AccountUserBean(ResponseStatus.ERROR, "User has no account");
	}

	/**
	 * User to bean
	 * 
	 * @param user
	 * @return
	 */
	private UserBean userToBean(User user) {
		UserBean bean = new UserBean();
		Profile profile = profileRepository.findByUser(user);
		bean.setStatus(ResponseStatus.SUCCESS);
		bean.setEmail(profile.getEmail());
		bean.setFullnames(profile.getFullnames());
		bean.setPhone(profile.getPhone());
		bean.setLogin(user.getLogin());
		bean.setId(user.getItemId());
		return bean;
	}

	/**
	 * Account user to bean
	 * 
	 * @param user
	 * @param accountUser
	 * @return
	 */
	private AccountUserBean accountUserToBean(AccountUser accountUser) {
		AccountUserBean bean = new AccountUserBean();
		Account account = accountUser.getAccount();
		User admin = userRepository.getOne(account.getAdmin().getId());
		bean.setStatus(ResponseStatus.SUCCESS);
		User user = accountUser.getUser();
		bean.setAdmin((admin.getId() == user.getId()));
		bean.setId(account.getItemId());
		bean.setCredits(account.getCredits());
		bean.setMessageCredits(account.getMessageCredits());
		bean.setName(account.getAccountName());
		bean.setAccountStatus(account.getAccountStatus().name());
		bean.setType(account.getAccountType().name());
		bean.setActive(account.getAccountStatus().equals(AccountStatus.Active));
		bean.setAcccountId(account.getItemId());
		return bean;
	}

}