package com.hubloy.sms.service.account;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.hubloy.sms.base.GenericMessage;
import com.hubloy.sms.beans.account.AccountBean;
import com.hubloy.sms.beans.account.AccountListBean;
import com.hubloy.sms.beans.account.AccountUserBean;
import com.hubloy.sms.beans.account.AccountUserListBean;
import com.hubloy.sms.beans.common.IntegerBean;
import com.hubloy.sms.constants.AccountRoles;
import com.hubloy.sms.constants.AccountStatus;
import com.hubloy.sms.constants.AccountType;
import com.hubloy.sms.constants.ResponseStatus;
import com.hubloy.sms.helpers.DateHelpers;
import com.hubloy.sms.helpers.IDHelpers;
import com.hubloy.sms.helpers.PaginationHelpers;
import com.hubloy.sms.orm.account.Account;
import com.hubloy.sms.orm.account.AccountUser;
import com.hubloy.sms.orm.account.Profile;
import com.hubloy.sms.orm.account.Role;
import com.hubloy.sms.orm.account.User;
import com.hubloy.sms.repository.account.AccountRepository;
import com.hubloy.sms.repository.account.AccountUserRepository;
import com.hubloy.sms.repository.account.ProfileRepository;
import com.hubloy.sms.repository.account.RoleRepository;
import com.hubloy.sms.repository.account.UserRepository;
import com.hubloy.sms.service.senders.MailSenderService;

@Service
public class AccountService {

	private final UserRepository userRepository;

	private final RoleRepository roleRepository;

	private final ProfileRepository profileRepository;

	private final AccountRepository accountRepository;

	private final AccountUserRepository accountUserRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private MailSenderService mailSenderService;

	@Autowired
	public AccountService(UserRepository userRepository, RoleRepository roleRepository,
			ProfileRepository profileRepository, AccountRepository accountRepository,
			AccountUserRepository accountUserRepository) {
		super();
		this.userRepository = userRepository;
		this.roleRepository = roleRepository;
		this.profileRepository = profileRepository;
		this.accountRepository = accountRepository;
		this.accountUserRepository = accountUserRepository;
	}

	/**
	 * List Accounts
	 * 
	 * @param page
	 * @return
	 */
	public AccountListBean listAccounts(int page) {
		AccountListBean bean = new AccountListBean();
		bean.setTotal(accountRepository.count());
		List<AccountBean> beans = new ArrayList<AccountBean>();
		Page<Account> accounts = accountRepository.findAll(PaginationHelpers.paginationIdDesc(page, 10));
		for (Account account : accounts) {
			beans.add(accountToBean(account));
		}
		bean.setRows(beans);
		return bean;
	}

	/**
	 * List Account users
	 * 
	 * @param id
	 * @param page
	 * @return
	 */
	public AccountUserListBean listAccountUsers(String id, int page) {
		AccountUserListBean bean = new AccountUserListBean();
		Account account = accountRepository.findByItemId(id);
		if (account != null) {
			bean.setTotal(accountUserRepository.countByAccount(account));
			List<AccountUserBean> beans = new ArrayList<AccountUserBean>();
			List<AccountUser> users = accountUserRepository.findByAccount(account,
					PaginationHelpers.paginationIdDesc(page, 10));
			for (AccountUser user : users) {
				beans.add(accountUserToBean(user));
			}
			bean.setRows(beans);
		}
		return bean;
	}

	/**
	 * Save Account
	 * 
	 * @param accountName
	 * @param name
	 * @param email
	 * @param login
	 * @param password
	 * @return
	 */
	public AccountBean saveAccount(String accountName, String name, String email, String login, String password) {
		AccountBean bean = new AccountBean();
		Set<Role> roles = new HashSet<Role>();
		roles.add(roleRepository.findByName("CUSTOMER"));
		User user = userRepository.findByLogin(login);
		Profile profile = profileRepository.findByEmail(email);
		if (profile != null) {
			bean.setStatus(ResponseStatus.ERROR);
			bean.setMessage("User with the same email already exists");
		} else {
			if (user == null) {
				user = new User();
				user.setLogin(login);
				user.setPassword(passwordEncoder.encode(password));
				user.setActive(true);
				user.setRoles(roles);
				User u = userRepository.saveAndFlush(user);
				profile = new Profile();
				profile.setUser(u);
				profile.setEmail(email);
				profile.setFullnames(name);
				profileRepository.saveAndFlush(profile);

				Account account = new Account();
				account.setAccountName(accountName);
				account.setAccountType(AccountType.User);
				account.setAdmin(u);
				account.setAccountStatus(AccountStatus.Active);
				Account a = accountRepository.saveAndFlush(account);

				AccountUser accountUser = new AccountUser();
				accountUser.setAccountRoles(AccountRoles.Super_Admin);
				accountUser.setAccount(a);
				accountUser.setUser(u);
				accountUserRepository.saveAndFlush(accountUser);
				bean = accountToBean(a);
			} else {
				bean.setStatus(ResponseStatus.ERROR);
				bean.setMessage("User with the same login already exists");
			}
		}
		return bean;
	}

	/**
	 * Get Account
	 * 
	 * @param id
	 * @return
	 */
	public AccountBean getAccount(String id) {
		AccountBean bean = new AccountBean();
		Account account = accountRepository.findByItemId(id);
		if (account != null) {
			bean = accountToBean(account);
		} else {
			bean.setStatus(ResponseStatus.ERROR);
			bean.setMessage("Account does not exist");
		}
		return bean;
	}

	/**
	 * Update Account
	 * 
	 * @param id
	 * @param name
	 * @param credits
	 * @param status
	 * @return
	 */
	public AccountBean updateAccount(String id, String name, Long credits, String status) {
		AccountBean bean = new AccountBean();
		Account account = accountRepository.findByItemId(id);
		if (account != null) {
			account.setAccountName(name);
			account.setCredits(credits);
			AccountStatus accountStatus = AccountStatus.valueOf(status);
			if (accountStatus != null) {
				account.setAccountStatus(accountStatus);
			}
			Account a = accountRepository.saveAndFlush(account);
			bean = accountToBean(a);
		} else {
			bean.setStatus(ResponseStatus.ERROR);
			bean.setMessage("Account does not exist");
		}
		return bean;
	}

	/**
	 * Toggle account status
	 * 
	 * @param id
	 * @return
	 */
	public GenericMessage toggle(String id) {
		Account account = accountRepository.findByItemId(id);
		if (account != null) {
			if (account.getAccountStatus().equals(AccountStatus.Active)) {
				account.setAccountStatus(AccountStatus.Deactivated);
				accountRepository.saveAndFlush(account);
				return new GenericMessage(ResponseStatus.SUCCESS, "Account deactivated");
			} else {
				account.setAccountStatus(AccountStatus.Active);
				accountRepository.saveAndFlush(account);
				return new GenericMessage(ResponseStatus.SUCCESS, "Account activated");
			}
		} else {
			return new GenericMessage(ResponseStatus.ERROR, "Account does not exist");
		}
	}

	/**
	 * Save Account user
	 * 
	 * @param id
	 * @param name
	 * @param email
	 * @param login
	 * @param password
	 * @return
	 */
	public GenericMessage saveAcountUser(String id, String name, String email, String login, String password) {
		Account account = accountRepository.findByItemId(id);
		if (account != null) {
			User user = userRepository.findByLogin(login);
			Profile profile = profileRepository.findByEmail(email);
			if (profile != null) {
				return new GenericMessage(ResponseStatus.ERROR, "Account with email exists");
			} else if (user != null) {
				return new GenericMessage(ResponseStatus.ERROR, "Account with login exists");
			} else {
				Set<Role> roles = new HashSet<Role>();
				roles.add(roleRepository.findByName("CUSTOMER"));
				user = new User();
				user.setLogin(login);
				user.setPassword(passwordEncoder.encode(password));
				user.setActive(true);
				user.setRoles(roles);
				User u = userRepository.saveAndFlush(user);
				profile = new Profile();
				profile.setUser(u);
				profile.setEmail(email);
				profile.setFullnames(name);
				profileRepository.saveAndFlush(profile);
				AccountUser accountUser = new AccountUser();
				accountUser.setAccountRoles(AccountRoles.Normal);
				accountUser.setUser(u);
				accountUser.setAccount(account);
				accountUserRepository.saveAndFlush(accountUser);
				return new GenericMessage(ResponseStatus.SUCCESS, "User added to account");
			}
		} else {
			return new GenericMessage(ResponseStatus.ERROR, "Account does not exist");
		}
	}

	/**
	 * Remove user from account
	 * 
	 * @param id
	 * @return
	 */
	public GenericMessage removeAccountUser(String id) {
		AccountUser user = accountUserRepository.findByItemId(id);
		if (user != null) {
			User u = user.getUser();
			Profile profile = profileRepository.findByUser(u);
			profileRepository.delete(profile);
			userRepository.delete(u);
			accountUserRepository.delete(user);
			return new GenericMessage(ResponseStatus.SUCCESS, "User removed from account");
		} else {
			return new GenericMessage(ResponseStatus.ERROR, "User does not exist");
		}
	}

	/**
	 * Get user by id
	 * 
	 * @param id
	 * @return
	 */
	public AccountUserBean getAccountUser(String id) {
		AccountUserBean bean = new AccountUserBean();
		AccountUser user = accountUserRepository.findByItemId(id);
		if (user != null) {
			bean = accountUserToBean(user);
		} else {
			bean.setStatus(ResponseStatus.ERROR);
			bean.setMessage("User does not exist");
		}
		return bean;
	}

	/**
	 * Update account
	 * 
	 * @param id
	 * @param name
	 * @param password
	 * @return
	 */
	public GenericMessage updateAccountUser(String id, String name, String password) {
		AccountUser user = accountUserRepository.findByItemId(id);
		if (user != null) {
			User u = user.getUser();
			if (password != null && !password.isEmpty()) {
				u.setPassword(passwordEncoder.encode(password));
				userRepository.saveAndFlush(u);
			}
			Profile profile = profileRepository.findByUser(u);
			profile.setFullnames(name);
			profileRepository.saveAndFlush(profile);
			user.setDateUpdated(new Date());
			accountUserRepository.saveAndFlush(user);
			return new GenericMessage(ResponseStatus.SUCCESS, "User updated");
		} else {
			return new GenericMessage(ResponseStatus.ERROR, "User does not exist");
		}
	}

	/**
	 * Save Account credits
	 * 
	 * @param user
	 * @param id
	 * @param total
	 * @return
	 */
	public GenericMessage saveAccountCredits(User user, String id, Long total) {
		Account account = accountRepository.findByItemId(id);
		if (account != null) {
			AccountUser accountUser = accountUserRepository.findByUser(user);
			if (accountUser != null) {
				Account parentAccount = accountRepository.findByAdmin(accountUser.getUser());
				if (parentAccount != null) {
					account.setCredits(total);
					accountRepository.saveAndFlush(account);
					return new GenericMessage(ResponseStatus.SUCCESS, "Credit applied");
				} else {
					return new GenericMessage(ResponseStatus.ERROR, "Action an only be done by admin");
				}
			} else {
				return new GenericMessage(ResponseStatus.ERROR, "User has no account");
			}
		} else {
			return new GenericMessage(ResponseStatus.ERROR, "Account does not exist");
		}
	}

	/**
	 * Get Account credits
	 * 
	 * @param id
	 * @return
	 */
	public IntegerBean getAccountCredits(String id) {
		IntegerBean bean = new IntegerBean();
		Account account = accountRepository.findByItemId(id);
		if (account != null && account.getCredits() != null) {
			bean.setNumber(account.getCredits());
		} else {
			bean.setNumber(0L);
		}
		return bean;
	}

	/**
	 * Total issues credits
	 * 
	 * @return
	 */
	public Long getTotalIssuedCredits() {
		return accountRepository.getTotalCredit();
	}

	/**
	 * Reset password
	 * 
	 * @param username
	 * @return
	 */
	public GenericMessage resetPassword(String username) {
		User user = userRepository.findByLogin(username);
		if (user != null) {
			String password = IDHelpers.generatePassword(username);
			user.setPassword(passwordEncoder.encode(password));
			userRepository.saveAndFlush(user);
			String body = "Hello,<br/>Your new password is " + password;
			mailSenderService.sendMail("no-reply@mobtechsolutions.com", user.getProfile().getEmail(), "Password Reset",
					body);
			return new GenericMessage(ResponseStatus.SUCCESS, "New password has been sent to your email");
		} else {
			return new GenericMessage(ResponseStatus.ERROR, "Invalid user");
		}

	}

	/**
	 * Account to bean
	 * 
	 * @param account
	 * @return
	 */
	private AccountBean accountToBean(Account account) {
		User admin = account.getAdmin();
		Profile profile = profileRepository.findByUser(admin);
		AccountBean bean = new AccountBean();
		bean.setId(account.getItemId());
		bean.setName(account.getAccountName());
		bean.setAccountStatus(account.getAccountStatus().name());
		bean.setCredits(account.getCredits());
		bean.setUsers(accountUserRepository.countByAccount(account));
		bean.setAdmin(profile.getFullnames());
		bean.setAdminId(admin.getItemId());
		bean.setSystem(account.getAccountType().equals(AccountType.System));
		bean.setStatus(ResponseStatus.SUCCESS);
		return bean;
	}

	/**
	 * Account user to bean
	 * 
	 * @param user
	 * @param accountUser
	 * @return
	 */
	private AccountUserBean accountUserToBean(AccountUser accountUser) {
		AccountUserBean bean = new AccountUserBean();
		boolean isAdmin = false;
		Account account = accountUser.getAccount();
		User user = accountUser.getUser();
		Profile profile = profileRepository.findByUser(user);
		if (account.getAdmin().getId() == user.getId()) {
			isAdmin = true;
		}
		bean.setStatus(ResponseStatus.SUCCESS);
		bean.setAdmin(isAdmin);
		bean.setId(accountUser.getItemId());
		bean.setCredits(account.getCredits());
		bean.setMessageCredits(account.getMessageCredits());
		bean.setName(profile.getFullnames());
		bean.setAccountStatus(account.getAccountStatus().name());
		bean.setType(account.getAccountType().name());
		bean.setActive(account.getAccountStatus().equals(AccountStatus.Active));
		bean.setAcccountId(account.getItemId());
		bean.setLogin(user.getLogin());
		bean.setCreatedAt(DateHelpers.formatToSQLDateString(accountUser.getDateCreated()));
		if (accountUser.getDateUpdated() != null) {
			bean.setUpdatedAt(DateHelpers.formatToSQLDateString(accountUser.getDateUpdated()));
		}
		bean.setEmail(profile.getEmail());
		return bean;
	}
}
