package com.hubloy.sms.service.message;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import javax.transaction.Transactional;

import org.apache.commons.io.LineIterator;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.africastalking.AfricasTalking;
import com.africastalking.SmsService;
import com.africastalking.sms.Recipient;
import com.hubloy.sms.base.GenericMessage;
import com.hubloy.sms.beans.message.MessageToSendBean;
import com.hubloy.sms.beans.message.MessageToSendListBean;
import com.hubloy.sms.beans.message.ReportMessageBean;
import com.hubloy.sms.constants.AccountType;
import com.hubloy.sms.constants.MessageStatus;
import com.hubloy.sms.constants.ResponseStatus;
import com.hubloy.sms.helpers.DateHelpers;
import com.hubloy.sms.helpers.DirectoryHelpers;
import com.hubloy.sms.helpers.FileUtils;
import com.hubloy.sms.helpers.PaginationHelpers;
import com.hubloy.sms.helpers.StringHelpers;
import com.hubloy.sms.orm.account.Account;
import com.hubloy.sms.orm.account.AccountUser;
import com.hubloy.sms.orm.account.User;
import com.hubloy.sms.orm.contacts.Contact;
import com.hubloy.sms.orm.contacts.ContactGroup;
import com.hubloy.sms.orm.message.MessageTemplate;
import com.hubloy.sms.orm.message.MessageToSend;
import com.hubloy.sms.orm.senders.SMSSender;
import com.hubloy.sms.orm.settings.SMSAPISettings;
import com.hubloy.sms.repository.account.AccountRepository;
import com.hubloy.sms.repository.account.AccountUserRepository;
import com.hubloy.sms.repository.contacts.ContactGroupRepository;
import com.hubloy.sms.repository.contacts.ContactRespository;
import com.hubloy.sms.repository.message.MessageTemplateRepository;
import com.hubloy.sms.repository.message.MessageToSendRepository;
import com.hubloy.sms.repository.senders.SMSSenderRepository;
import com.hubloy.sms.repository.settings.SMSAPISettingsRepository;
import com.hubloy.sms.view.ExcelView;

@Service
public class SMSService {

	@Value("${app.ctx}")
	private String path;

	private final MessageToSendRepository messageToSendRepository;

	private final MessageTemplateRepository messageTemplateRepository;

	private final ContactGroupRepository contactGroupRepository;

	private final AccountUserRepository accountUserRepository;

	private final ContactRespository contactRespository;

	private final SMSSenderRepository smsSenderRepository;

	private final SMSAPISettingsRepository smsapiSettingsRepository;

	private final AccountRepository accountRepository;

	@Autowired
	public SMSService(MessageToSendRepository messageToSendRepository,
			MessageTemplateRepository messageTemplateRepository, ContactGroupRepository contactGroupRepository,
			AccountUserRepository accountUserRepository, ContactRespository contactRespository,
			SMSSenderRepository smsSenderRepository, SMSAPISettingsRepository smsapiSettingsRepository,
			AccountRepository accountRepository) {
		super();
		this.messageToSendRepository = messageToSendRepository;
		this.messageTemplateRepository = messageTemplateRepository;
		this.contactGroupRepository = contactGroupRepository;
		this.accountUserRepository = accountUserRepository;
		this.contactRespository = contactRespository;
		this.smsSenderRepository = smsSenderRepository;
		this.smsapiSettingsRepository = smsapiSettingsRepository;
		this.accountRepository = accountRepository;
	}

	/**
	 * List Messages
	 * 
	 * @param user
	 * @param page
	 * @return
	 */
	public MessageToSendListBean listMessages(User user, int page) {
		MessageToSendListBean bean = new MessageToSendListBean();
		AccountUser accountUser = accountUserRepository.findByUser(user);
		if (accountUser != null) {
			Account account = accountUser.getAccount();
			List<MessageToSendBean> rows = new ArrayList<MessageToSendBean>();
			List<MessageToSend> messages = messageToSendRepository.findByAccount(account,
					PaginationHelpers.paginationIdDesc(page, 10));
			for (MessageToSend message : messages) {
				rows.add(messageToBean(message));
			}
			bean.setTotal(messageToSendRepository.countByAccount(account));
			bean.setRows(rows);
		}
		return bean;
	}

	/**
	 * Send Single message
	 * 
	 * @param user
	 * @param source
	 * @param contacts
	 * @param group
	 * @param sender
	 * @param schedule
	 * @param message
	 * @return
	 */
	@Async("asyncExecutor")
	@Transactional
	public CompletableFuture<GenericMessage> sendSingleSMS(User user, String source, String contacts, String group,
			String sender, String schedule, String message) {
		AccountUser accountUser = accountUserRepository.findByUser(user);
		if (accountUser != null) {
			Account account = accountUser.getAccount();
			MessageToSend messageToSend = new MessageToSend();
			messageToSend.setAccount(account);
			messageToSend.setMessageStatus(MessageStatus.Pending);
			if (source.equalsIgnoreCase("contacts")) {
				messageToSend.setMsisdn(contacts);
			} else {
				ContactGroup contactGroup = contactGroupRepository.findByItemId(group);
				if (contactGroup != null) {
					messageToSend.setContactGroup(contactGroup);
				} else {
					return CompletableFuture
							.completedFuture(new GenericMessage(ResponseStatus.ERROR, "Group does not exist"));
				}
			}
			SMSSender smsSender = smsSenderRepository.findByItemId(sender);
			if (smsSender != null) {
				messageToSend.setSmsSender(smsSender);
			} else {
				return CompletableFuture
						.completedFuture(new GenericMessage(ResponseStatus.ERROR, "Sender does not exist"));
			}

			boolean sendNow = true;
			if (schedule != null && !schedule.isEmpty()) {
				messageToSend.setSchedule(DateHelpers.formatToSQLDate(schedule));
				sendNow = false;
			} else {
				messageToSend.setSchedule(new Date());
			}
			messageToSend.setMessage(message);
			MessageToSend toSend = messageToSendRepository.saveAndFlush(messageToSend);
			List<SMSAPISettings> smsapiSettings = smsapiSettingsRepository.findAll();
			SMSAPISettings settings = smsapiSettings.get(0);
			AfricasTalking.initialize(settings.getUsername(), settings.getApiKey());
			processMessage(toSend, settings, sendNow);
			return CompletableFuture.completedFuture(new GenericMessage(ResponseStatus.SUCCESS, "Message sent"));
		} else {
			return CompletableFuture.completedFuture(new GenericMessage(ResponseStatus.ERROR, "User has no account"));
		}
	}

	/**
	 * Bulk upload process
	 * 
	 * @param user
	 * @param file
	 * @param sender
	 * @param schedule
	 * @param template
	 * @return
	 */
	@Async("asyncExecutor")
	@Transactional
	public CompletableFuture<GenericMessage> bulkCSVUpload(User user, MultipartFile file, String sender,
			String schedule, String template) {
		AccountUser accountUser = accountUserRepository.findByUser(user);
		if (accountUser != null) {
			Account account = accountUser.getAccount();
			DirectoryHelpers.setDirectories(path);
			String path = DirectoryHelpers.getMessageDirectory();
			SMSSender smsSender = smsSenderRepository.findByItemId(sender);
			if (smsSender == null) {
				return CompletableFuture
						.completedFuture(new GenericMessage(ResponseStatus.ERROR, "Sender does not exist"));
			}
			MessageTemplate messageTemplate = messageTemplateRepository.findByItemId(template);
			if (messageTemplate == null) {
				return CompletableFuture
						.completedFuture(new GenericMessage(ResponseStatus.ERROR, "Message template does not exist"));
			}
			boolean sendNow = true;
			Date scheduleDate = new Date();
			if (schedule != null && !schedule.isEmpty()) {
				scheduleDate = DateHelpers.formatToSQLDate(schedule);
				sendNow = false;
			}
			JSONObject jsonObject = FileUtils.uploadCSV(path, file);
			if (jsonObject.length() > 0) {
				String filePath = jsonObject.getString("path");
				File uploadedFile = new File(filePath);
				try {
					String message = messageTemplate.getTemplate();
					LineIterator it = org.apache.commons.io.FileUtils.lineIterator(uploadedFile, "UTF-8");
					String line = null;
					String[] parts = null;
					MessageToSend messageToSend = null;
					List<MessageToSend> list = new ArrayList<MessageToSend>();
					String modifiedMessage = null;
					while (it.hasNext()) {
						line = it.nextLine();
						parts = StringHelpers.splitString(line);
						if (parts.length > 1) {
							modifiedMessage = message;
							modifiedMessage = modifiedMessage.replace("{name}", parts[0]);
							modifiedMessage = modifiedMessage.replace("{phone}", parts[1]);
							modifiedMessage = modifiedMessage.replace("{number}", parts[1]);
							if (parts.length >= 3) {
								modifiedMessage = modifiedMessage.replace("{amount}", parts[2]);
							}
							messageToSend = new MessageToSend();
							messageToSend.setMsisdn(parts[1]);
							messageToSend.setAccount(account);
							messageToSend.setResponse("Queued for processing");
							messageToSend.setMessageStatus(MessageStatus.Pending);
							messageToSend.setSmsSender(smsSender);
							messageToSend.setSchedule(scheduleDate);
							messageToSend.setMessage(modifiedMessage);
							list.add(messageToSend);
						} else {

						}
					}
					if (!list.isEmpty()) {
						uploadedFile.delete();
						if (sendNow) {
							List<SMSAPISettings> smsapiSettings = smsapiSettingsRepository.findAll();
							if (!list.isEmpty()) {
								SMSAPISettings settings = smsapiSettings.get(0);
								AfricasTalking.initialize(settings.getUsername(), settings.getApiKey());

								for (MessageToSend toSend : list) {
									processMessage(toSend, settings, true);
								}

							}
							return CompletableFuture.completedFuture(
									new GenericMessage(ResponseStatus.SUCCESS, list.size() + " messages sent"));
						} else {
							for (MessageToSend toSend : list) {
								messageToSendRepository.saveAndFlush(toSend);
							}
							return CompletableFuture.completedFuture(
									new GenericMessage(ResponseStatus.SUCCESS, list.size() + " messages scheduled"));
						}

					} else {
						return CompletableFuture.completedFuture(new GenericMessage(ResponseStatus.ERROR,
								"No messages to save for processing. Please check the file format"));
					}
				} catch (IOException e) {
					return CompletableFuture.completedFuture(
							new GenericMessage(ResponseStatus.ERROR, "Error processing file " + e.getMessage()));
				}
			} else {
				return CompletableFuture
						.completedFuture(new GenericMessage(ResponseStatus.ERROR, "Error uploading file"));
			}
		} else {
			return CompletableFuture.completedFuture(new GenericMessage(ResponseStatus.ERROR, "User has no account"));
		}
	}

	@Async("asyncExecutor")
	@Transactional
	public CompletableFuture<GenericMessage> bulkExcelUpload(User user, MultipartFile file, String sender,
			String schedule, String template) {
		AccountUser accountUser = accountUserRepository.findByUser(user);
		if (accountUser != null) {
			Account account = accountUser.getAccount();
			DirectoryHelpers.setDirectories(path);
			String path = DirectoryHelpers.getMessageDirectory();
			SMSSender smsSender = smsSenderRepository.findByItemId(sender);
			if (smsSender == null) {
				return CompletableFuture
						.completedFuture(new GenericMessage(ResponseStatus.ERROR, "Sender does not exist"));
			}
			MessageTemplate messageTemplate = messageTemplateRepository.findByItemId(template);
			if (messageTemplate == null) {
				return CompletableFuture
						.completedFuture(new GenericMessage(ResponseStatus.ERROR, "Message template does not exist"));
			}
			boolean sendNow = true;
			Date scheduleDate = new Date();
			if (schedule != null && !schedule.isEmpty()) {
				scheduleDate = DateHelpers.formatToSQLDate(schedule);
				sendNow = false;
			}
			JSONObject jsonObject = FileUtils.uploadExcel(path, file);
			if (jsonObject.length() > 0) {
				String filePath = jsonObject.getString("path");
				File uploadedFile = new File(filePath);
				FileInputStream fileInputStream = null;
				HSSFWorkbook workbook = null;
				try {
					fileInputStream = new FileInputStream(uploadedFile);
					workbook = new HSSFWorkbook(fileInputStream);
					HSSFSheet sheet = workbook.getSheetAt(0);
					Cell name = null;
					Cell phone = null;
					Cell amount = null;
					String phonenumber = null;
					String amountValue = null;
					String message = messageTemplate.getTemplate();
					MessageToSend messageToSend = null;
					List<MessageToSend> list = new ArrayList<MessageToSend>();
					String modifiedMessage = null;
					DataFormatter formatter = new DataFormatter();
					for (Row row : sheet) {
						name = row.getCell(0);
						phone = row.getCell(1);
						amount = row.getCell(2);
						if (name != null && phone != null) {
							if (phone.getCellType() == CellType.NUMERIC) {
								phonenumber = formatter.formatCellValue(phone) + "";
							} else {
								phonenumber = phone.getStringCellValue();
							}
							if (!phonenumber.startsWith("+")) {
								phonenumber = "+" + phonenumber;
							}
							modifiedMessage = message;
							modifiedMessage = modifiedMessage.replace("{name}", name.getStringCellValue());
							modifiedMessage = modifiedMessage.replace("{phone}", phonenumber);
							modifiedMessage = modifiedMessage.replace("{number}", phonenumber);
							if (amount != null) {
								if (phone.getCellType() == CellType.NUMERIC) {
									amountValue = amount.getNumericCellValue() + "";
								} else {
									amountValue = amount.getStringCellValue();
								}
								amountValue = StringHelpers.formatCurrency(Float.valueOf(amountValue));
								modifiedMessage = modifiedMessage.replace("{amount}", amountValue);
							}
							messageToSend = new MessageToSend();
							messageToSend.setMsisdn(phonenumber);
							messageToSend.setAccount(account);
							messageToSend.setResponse("Queued for processing");
							messageToSend.setMessageStatus(MessageStatus.Pending);
							messageToSend.setSmsSender(smsSender);
							messageToSend.setSchedule(scheduleDate);
							messageToSend.setMessage(modifiedMessage);
							list.add(messageToSend);
						}
					}
					workbook.close();
					fileInputStream.close();
					workbook = null;
					fileInputStream = null;
					if (!list.isEmpty()) {
						uploadedFile.delete();
						if (sendNow) {
							List<SMSAPISettings> smsapiSettings = smsapiSettingsRepository.findAll();
							if (!list.isEmpty()) {
								SMSAPISettings settings = smsapiSettings.get(0);
								AfricasTalking.initialize(settings.getUsername(), settings.getApiKey());

								for (MessageToSend toSend : list) {
									processMessage(toSend, settings, true);
								}

							}
							return CompletableFuture.completedFuture(
									new GenericMessage(ResponseStatus.SUCCESS, list.size() + " messages sent"));
						} else {
							for (MessageToSend toSend : list) {
								messageToSendRepository.saveAndFlush(toSend);
							}
							return CompletableFuture.completedFuture(
									new GenericMessage(ResponseStatus.SUCCESS, list.size() + " messages scheduled"));
						}

					} else {
						return CompletableFuture.completedFuture(new GenericMessage(ResponseStatus.ERROR,
								"No messages to save for processing. Please check the file format"));
					}
				} catch (IOException e) {
					return CompletableFuture.completedFuture(
							new GenericMessage(ResponseStatus.ERROR, "Error processing file " + e.getMessage()));
				}
			} else {
				return CompletableFuture
						.completedFuture(new GenericMessage(ResponseStatus.ERROR, "Error uploading file"));
			}
		} else {
			return CompletableFuture.completedFuture(new GenericMessage(ResponseStatus.ERROR, "User has no account"));
		}
	}

	/**
	 * Process
	 */
	public void process() {
		List<MessageToSend> list = messageToSendRepository.findByMessageStatusAndSchedule(MessageStatus.Pending,
				PaginationHelpers.paginationIdDesc(0, 100));
		List<SMSAPISettings> smsapiSettings = smsapiSettingsRepository.findAll();
		if (!list.isEmpty()) {
			SMSAPISettings settings = smsapiSettings.get(0);
			AfricasTalking.initialize(settings.getUsername(), settings.getApiKey());
			for (MessageToSend toSend : list) {
				processMessage(toSend, settings, true);
			}
		}
	}

	@Transactional
	public void processMessage(MessageToSend messageToSend, SMSAPISettings settings, boolean sendNow) {
		if (sendNow) {
			try {
				Account account = accountRepository.findByItemId(messageToSend.getAccount().getItemId());
				Long credits = account.getCredits();
				if (account.getAccountType().equals(AccountType.System)) {
					credits = settings.getCredits();
				}
				if (credits == null) {
					credits = 0L;
				}
				if (credits > 0) {
					messageToSend.setMessageStatus(MessageStatus.Processing);
					MessageToSend toSend = messageToSendRepository.saveAndFlush(messageToSend);
					SmsService service = AfricasTalking.getService(AfricasTalking.SERVICE_SMS);
					if (messageToSend.getContactGroup() != null) {

						ContactGroup contactGroup = contactGroupRepository
								.findByItemId(messageToSend.getContactGroup().getItemId());
						List<Contact> contacts = contactRespository.findByContactGroup(contactGroup);
						Long contactsNumber = contactRespository.countByContactGroup(contactGroup);
						long totalToSend = contactRespository.countByContactGroup(contactGroup);
						long messagesToSend = totalToSend;
						int total = 1;
						for (int i = 0; i < contactsNumber; i++) {
							if (messageToSend.getSmsSender() != null) {
								total = StringHelpers.countSMSMessages(messageToSend.getMessage(), 160);
								if (total > 1) {
									messagesToSend += total - 1; // subtract 1 as we already counted it as one
								}
							}
						}

						if (credits >= messagesToSend) {
							try {
								String phone = null;
								List<Recipient> response = null;
								String modifiedMessage = null;
								for (Contact contact : contacts) {
									phone = contact.getPhone();
									if (!phone.startsWith("+")) {
										phone = "+" + phone.trim();
									}
									if (messageToSend.getSmsSender() != null) {
										modifiedMessage = messageToSend.getMessage();
										modifiedMessage = modifiedMessage.replace("{name}", contact.getName());
										modifiedMessage = modifiedMessage.replace("{phone}", phone);
										modifiedMessage = modifiedMessage.replace("{number}", phone);
										response = service.send(modifiedMessage,
												messageToSend.getSmsSender().getSender(),
												StringHelpers.splitString(phone), false);
										if (response != null) {
											for(Recipient recipient : response) {
												if (recipient.status.equalsIgnoreCase("success")) {
													int messages = StringHelpers.countSMSMessages(messageToSend.getMessage(), 160);
													credits = credits - messages;
												}
											}
										}
										response = null;
									}
								}								
								account.setCredits(credits);
								accountRepository.saveAndFlush(account);
								toSend.setResponse("Message Sent");
								toSend.setMessageStatus(MessageStatus.Sent);
								messageToSendRepository.saveAndFlush(toSend);
							} catch (IOException e) {
								toSend.setMessageStatus(MessageStatus.Error);
								toSend.setResponse("Error " + e.getMessage());
								messageToSendRepository.saveAndFlush(toSend);
							}
						} else {
							toSend.setMessageStatus(MessageStatus.Error);
							toSend.setResponse("Insufficient credits " + credits + " to send " + messagesToSend);
							messageToSendRepository.saveAndFlush(toSend);
						}
					} else {
						int total = StringHelpers.countSMSMessages(messageToSend.getMessage(), 160);
						String modifiedMessage = null;
						if (credits >= total) {
							try {
								String phone = messageToSend.getMsisdn();
								if (!phone.startsWith("+")) {
									phone = "+" + phone.trim();
								}
								List<Recipient> response = null;
								if (messageToSend.getSmsSender() != null) {
									modifiedMessage = messageToSend.getMessage();
									modifiedMessage = modifiedMessage.replace("{phone}", phone);
									modifiedMessage = modifiedMessage.replace("{number}", phone);
									response = service.send(modifiedMessage, messageToSend.getSmsSender().getSender(),
											StringHelpers.splitString(phone), false);
									if (response != null) {
										for(Recipient recipient : response) {
											if (recipient.status.equalsIgnoreCase("success")) {
												int messages = StringHelpers.countSMSMessages(messageToSend.getMessage(), 160);
												credits = credits - messages;
											}
										}
									}
									response = null;
									account.setCredits(credits);
									accountRepository.saveAndFlush(account);
									toSend.setResponse("Message Sent");
									toSend.setMessageStatus(MessageStatus.Sent);
								} else {
									toSend.setResponse("Sender does not exist");
									toSend.setMessageStatus(MessageStatus.Error);
								}
								messageToSendRepository.saveAndFlush(toSend);
							} catch (IOException e) {
								toSend.setMessageStatus(MessageStatus.Error);
								toSend.setResponse("Error " + e.getMessage());
								messageToSendRepository.saveAndFlush(toSend);
							}
						} else {
							toSend.setMessageStatus(MessageStatus.Error);
							toSend.setResponse("Insufficient credits to send " + total + " messages");
							messageToSendRepository.saveAndFlush(toSend);
						}
					}
				} else {
					messageToSend.setMessageStatus(MessageStatus.Error);
					messageToSend.setResponse("Insufficient credits");
					messageToSendRepository.saveAndFlush(messageToSend);
				}
			} catch (Exception e) {
			}
		}
	}

	/**
	 * Generate Report Model
	 * 
	 * @param user
	 * @param start
	 * @param end
	 * @return
	 */
	public ModelAndView generateReport(String accountId, String start, String end) {
		ModelAndView modelAndView = new ModelAndView();
		List<ReportMessageBean> list = new ArrayList<ReportMessageBean>();
		Account account = accountRepository.findByItemId(accountId);
		if (account != null) {
			List<MessageToSend> toSends = null;
			if ((start != null && !start.isEmpty()) && (end != null && !end.isEmpty())) {
				Date startDate = DateHelpers.formatToSQLDate(start);
				Date endDate = DateHelpers.formatToSQLDate(end);
				toSends = messageToSendRepository.findByAccountAndDate(account, startDate, endDate);
			} else if ((start != null && !start.isEmpty())) {
				Date startDate = DateHelpers.formatToSQLDate(start);
				toSends = messageToSendRepository.findByAccountAndStartDate(account, startDate);
			} else if ((end != null && !end.isEmpty())) {
				Date endDate = DateHelpers.formatToSQLDate(end);
				toSends = messageToSendRepository.findByAccountAndEndDate(account, endDate);
			} else {
				toSends = messageToSendRepository.findByAccount(account, PaginationHelpers.paginationIdDesc(0, 100));
			}
			List<Contact> contacts = null;
			for (MessageToSend message : toSends) {
				if (message.getContactGroup() != null) {
					contacts = contactRespository.findByContactGroup(message.getContactGroup());
					for (Contact contact : contacts) {
						list.add(messageToReportBean(message, contact.getPhone()));
					}
				} else if (message.getMsisdn() != null) {
					list.add(messageToReportBean(message, message.getMsisdn()));
				} else {
					list.add(messageToReportBean(message, "N/A"));
				}
			}
			modelAndView.addObject("account", account.getAccountName());
			modelAndView.addObject("messages", list);
			modelAndView.setView(new ExcelView());
		}
		return modelAndView;
	}

	/**
	 * Message To bean
	 * 
	 * @param message
	 * @return
	 */
	private MessageToSendBean messageToBean(MessageToSend message) {
		MessageToSendBean bean = new MessageToSendBean();
		bean.setId(message.getItemId());
		bean.setMessageStatus(message.getMessageStatus().name());
		if (message.getMessageTemplate() != null) {
			bean.setMessage(message.getMessageTemplate().getTemplate());
		} else if (message.getMessage() != null) {
			bean.setMessage(message.getMessage());
		} else {
			bean.setMessage("N/A");
		}
		if (message.getContactGroup() != null) {
			bean.setContacts(contactRespository.countByContactGroup(message.getContactGroup()));
		} else if (message.getMsisdn() != null) {
			bean.setContacts(1L);
		} else {
			bean.setContacts(0L);
		}
		bean.setCreatedAt(DateHelpers.formatToSQLDateString(message.getDateCreated()));
		if (message.getSchedule() != null) {
			bean.setSchedule(DateHelpers.formatToSQLDateString(message.getSchedule()));
		} else {
			bean.setSchedule("N/A");
		}
		if (message.getDateUpdated() != null) {
			bean.setUpdatedAt(DateHelpers.formatToSQLDateString(message.getDateUpdated()));
		} else {
			bean.setUpdatedAt("N/A");
		}
		if (message.getResponse() != null) {
			bean.setResponse(message.getResponse());
		} else {
			bean.setResponse("N/A");
		}
		if (message.getSmsSender() != null) {
			bean.setSender(message.getSmsSender().getSender());
		} else {
			bean.setSender("N/A or Deleted");
		}

		return bean;
	}

	/**
	 * Message to bean
	 * 
	 * @param message
	 * @param number
	 * @return
	 */
	private ReportMessageBean messageToReportBean(MessageToSend message, String number) {
		ReportMessageBean bean = new ReportMessageBean();
		if (message.getMessageTemplate() != null) {
			bean.setMessage(message.getMessageTemplate().getTemplate());
		} else if (message.getMessage() != null) {
			bean.setMessage(message.getMessage());
		} else {
			bean.setMessage("N/A");
		}
		bean.setNumber(number);
		bean.setCreatedAt(DateHelpers.formatToSQLDateString(message.getDateCreated()));
		if (message.getSchedule() != null) {
			bean.setSchedule(DateHelpers.formatToSQLDateString(message.getSchedule()));
		} else {
			bean.setSchedule("N/A");
		}
		if (message.getDateUpdated() != null) {
			bean.setUpdatedAt(DateHelpers.formatToSQLDateString(message.getDateUpdated()));
		} else {
			bean.setUpdatedAt("N/A");
		}
		if (message.getResponse() != null) {
			bean.setResponse(message.getResponse());
		} else {
			bean.setResponse("N/A");
		}
		if (message.getSmsSender() != null) {
			bean.setSender(message.getSmsSender().getSender());
		} else {
			bean.setSender("N/A or Deleted");
		}
		return bean;
	}
}
