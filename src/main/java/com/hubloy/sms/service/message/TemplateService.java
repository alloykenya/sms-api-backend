package com.hubloy.sms.service.message;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hubloy.sms.base.GenericMessage;
import com.hubloy.sms.beans.message.TemplateBean;
import com.hubloy.sms.beans.message.TemplateListBean;
import com.hubloy.sms.constants.ResponseStatus;
import com.hubloy.sms.helpers.DateHelpers;
import com.hubloy.sms.helpers.PaginationHelpers;
import com.hubloy.sms.orm.account.Account;
import com.hubloy.sms.orm.account.AccountUser;
import com.hubloy.sms.orm.account.User;
import com.hubloy.sms.orm.message.MessageTemplate;
import com.hubloy.sms.repository.account.AccountUserRepository;
import com.hubloy.sms.repository.message.MessageTemplateRepository;

@Service
public class TemplateService {

	private final MessageTemplateRepository messageTemplateRepository;

	private final AccountUserRepository accountUserRepository;

	@Autowired
	public TemplateService(MessageTemplateRepository messageTemplateRepository,
			AccountUserRepository accountUserRepository) {
		super();
		this.messageTemplateRepository = messageTemplateRepository;
		this.accountUserRepository = accountUserRepository;
	}

	/**
	 * List Templates
	 * 
	 * @param user
	 * @param page
	 * @return
	 */
	public TemplateListBean listTemplates(User user, int page) {
		TemplateListBean bean = new TemplateListBean();
		AccountUser accountUser = accountUserRepository.findByUser(user);
		if (accountUser != null) {
			Account account = accountUser.getAccount();
			bean.setTotal(messageTemplateRepository.countByAccount(account));
			List<TemplateBean> list = new ArrayList<TemplateBean>();
			List<MessageTemplate> templates = messageTemplateRepository.findByAccount(account,
					PaginationHelpers.paginationIdDesc(page, 10));
			for(MessageTemplate template : templates) {
				list.add(templateToBean(template));
			}
			bean.setRows(list);
		}
		return bean;
	}
	
	/**
	 * List All account templates
	 * 
	 * @param user
	 * @return
	 */
	public List<TemplateBean> listAllTemplates(User user){
		List<TemplateBean> list = new ArrayList<TemplateBean>();
		AccountUser accountUser = accountUserRepository.findByUser(user);
		if (accountUser != null) {
			Account account = accountUser.getAccount();
			List<MessageTemplate> templates = messageTemplateRepository.findByAccount(account);
			for(MessageTemplate template : templates) {
				list.add(templateToBean(template));
			}
		}
		return list;
	}
	
	/**
	 * Save Template
	 * 
	 * @param user
	 * @param name
	 * @param template
	 * @return
	 */
	public GenericMessage saveTemplate(User user, String name, String template) {
		AccountUser accountUser = accountUserRepository.findByUser(user);
		if (accountUser != null) {
			Account account = accountUser.getAccount();
			MessageTemplate messageTemplate = new MessageTemplate();
			messageTemplate.setAccount(account);
			messageTemplate.setName(name);
			messageTemplate.setTemplate(template);
			messageTemplateRepository.saveAndFlush(messageTemplate);
			return new GenericMessage(ResponseStatus.SUCCESS, "Template saved");
		} else {
			return new GenericMessage(ResponseStatus.ERROR, "User has no account");
		}
	}
	
	/**
	 * Delete Template
	 * 
	 * @param id
	 * @return
	 */
	public GenericMessage deleteTemplate(String id) {
		MessageTemplate messageTemplate = messageTemplateRepository.findByItemId(id);
		if(messageTemplate != null) {
			messageTemplateRepository.delete(messageTemplate);
			return new GenericMessage(ResponseStatus.SUCCESS, "Template deleted");
		}else {
			return new GenericMessage(ResponseStatus.ERROR, "Message template does not exist");
		}
	}

	/**
	 * Template to bean
	 * 
	 * @param template
	 * @return
	 */
	private TemplateBean templateToBean(MessageTemplate template) {
		TemplateBean bean = new TemplateBean();
		bean.setId(template.getItemId());
		bean.setName(template.getName());
		bean.setTemplate(template.getTemplate());
		bean.setCreatedAt(DateHelpers.formatToSQLDateString(template.getDateCreated()));
		return bean;
	}
}
