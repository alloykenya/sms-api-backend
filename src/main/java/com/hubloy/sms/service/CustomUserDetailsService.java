package com.hubloy.sms.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.hubloy.sms.orm.account.User;
import com.hubloy.sms.repository.account.UserRepository;

/**
 * Oauth User Details Service
 * 
 * @author Paul
 *
 */
@Service
public class CustomUserDetailsService implements UserDetailsService {

	private final UserRepository userRepository;

	@Autowired
	public CustomUserDetailsService(UserRepository userRepository) {

		this.userRepository = userRepository;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		User user = userRepository.findByLogin(username);
		if (user == null) {
			throw new UsernameNotFoundException(String.format("API key %s does not exist!", username));
		} else
			return new UserRepositoryUserDetails(user);
	}

	public final static class UserRepositoryUserDetails extends User implements UserDetails {

		private static final long serialVersionUID = 1L;

		public UserRepositoryUserDetails(User user) {
			super(user);
		}

		@Override
		public Collection<? extends GrantedAuthority> getAuthorities() {
			return getRoles();
		}

		@Override
		public String getUsername() {
			return getLogin();
		}

		@Override
		public boolean isAccountNonExpired() {
			return isActive();
		}

		@Override
		public boolean isAccountNonLocked() {
			return isActive();
		}

		@Override
		public boolean isCredentialsNonExpired() {
			return isActive();
		}

		@Override
		public boolean isEnabled() {
			return isActive();
		}

	}

}
