package com.hubloy.sms.service.settings;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.africastalking.AfricasTalking;
import com.africastalking.ApplicationService;
import com.africastalking.application.ApplicationResponse;
import com.hubloy.sms.base.GenericMessage;
import com.hubloy.sms.beans.settings.ApiSettingBean;
import com.hubloy.sms.beans.settings.CreditsBean;
import com.hubloy.sms.constants.ResponseStatus;
import com.hubloy.sms.orm.account.User;
import com.hubloy.sms.orm.settings.SMSAPISettings;
import com.hubloy.sms.repository.settings.SMSAPISettingsRepository;
import com.hubloy.sms.service.account.AccountService;

@Service
public class SMSAccountService {

	private final SMSAPISettingsRepository smsapiSettingsRepository;

	@Autowired
	private AccountService accountService;

	@Autowired
	public SMSAccountService(SMSAPISettingsRepository smsapiSettingsRepository) {
		super();
		this.smsapiSettingsRepository = smsapiSettingsRepository;
	}

	/**
	 * Get Settings
	 * 
	 * @return
	 */
	public ApiSettingBean getSettings() {
		ApiSettingBean bean = new ApiSettingBean();
		List<SMSAPISettings> list = smsapiSettingsRepository.findAll();
		if (!list.isEmpty()) {
			SMSAPISettings settings = list.get(0);
			bean = settingToBean(settings);
		}
		return bean;
	}

	/**
	 * Save Settings
	 * 
	 * @param username
	 * @param apiKey
	 * @return
	 */
	public GenericMessage saveSettings(User user, String username, String apiKey, Long rate) {
		SMSAPISettings settings = null;
		List<SMSAPISettings> list = smsapiSettingsRepository.findAll();
		if (!list.isEmpty()) {
			settings = list.get(0);
		} else {
			settings = new SMSAPISettings();
		}
		if (rate == null) {
			rate = 1L;
		}
		settings.setUsername(username);
		settings.setApiKey(apiKey);
		String amount = getCredits(username, apiKey);
		String[] balance = amount.split("\\s+");
		Long credits = 0L;
		if (balance.length > 1) {
			credits = ( (long) Float.parseFloat(balance[1]) ) / rate;
		}
		settings.setCredits(credits);
		settings.setRate(rate);
		settings.setAmount(amount);
		smsapiSettingsRepository.saveAndFlush(settings);
		return new GenericMessage(ResponseStatus.SUCCESS, "Settings Saved");
	}

	/**
	 * Refresh Balance
	 * 
	 * @param user
	 * @return
	 */
	public GenericMessage refreshBalance() {
		SMSAPISettings settings = null;
		List<SMSAPISettings> list = smsapiSettingsRepository.findAll();
		if (!list.isEmpty()) {
			settings = list.get(0);
			String amount = getCredits(settings.getUsername(), settings.getApiKey());
			String[] balance = amount.split("\\s+");
			Long credits = 0L;
			if (balance.length > 1) {
				credits = ( (long) Float.parseFloat(balance[1]) ) / settings.getRate();
			}
			settings.setCredits(credits);
			settings.setAmount(amount);
			smsapiSettingsRepository.saveAndFlush(settings);
			return new GenericMessage(ResponseStatus.SUCCESS, "Balance Updated");
		} else {
			return new GenericMessage(ResponseStatus.ERROR, "No settings found");
		}
	}

	/**
	 * Returns credit status
	 * 
	 * @return
	 */
	public CreditsBean getTotalAvailableCredits() {
		CreditsBean bean = new CreditsBean();
		List<SMSAPISettings> list = smsapiSettingsRepository.findAll();
		if (!list.isEmpty()) {
			SMSAPISettings settings = list.get(0);
			Long issued = accountService.getTotalIssuedCredits();
			if(issued == null) {
				issued = 0L;
			}
			Long total = 0L;
			if(settings.getCredits() != null) {
				total = settings.getCredits();
			}
			bean.setTotal(total);
			bean.setIssued(issued);
			bean.setAvailable(total - issued);
			bean.setCredits(settings.getAmount());
		}
		return bean;
	}

	/**
	 * Get Credits
	 * 
	 * @param username
	 * @param apiKey
	 * @return
	 */
	private String getCredits(String username, String apiKey) {
		AfricasTalking.initialize(username, apiKey);
		ApplicationService applicationService = AfricasTalking.getService(AfricasTalking.SERVICE_APPLICATION);
		ApplicationResponse response;
		try {
			response = applicationService.fetchApplicationData();
			String b = response.userData.balance;
			if (b != null) {
				return b;
			} else {
				return "0";
			}

		} catch (IOException e) {
		}
		return "0";
	}

	/**
	 * Setting to bean
	 * 
	 * @param settings
	 * @return
	 */
	private ApiSettingBean settingToBean(SMSAPISettings settings) {
		ApiSettingBean bean = new ApiSettingBean();
		bean.setStatus(ResponseStatus.SUCCESS);
		bean.setActive(settings.isActive());
		bean.setApiKey(settings.getApiKey());
		bean.setCredits(settings.getCredits());
		bean.setId(settings.getItemId());
		bean.setUsername(settings.getUsername());
		bean.setRate(settings.getRate());
		return bean;
	}
}
