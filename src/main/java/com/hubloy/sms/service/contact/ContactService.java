package com.hubloy.sms.service.contact;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import javax.transaction.Transactional;

import org.apache.commons.io.LineIterator;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.hubloy.sms.base.GenericMessage;
import com.hubloy.sms.beans.contact.ContactBean;
import com.hubloy.sms.beans.contact.ContactListBean;
import com.hubloy.sms.beans.contact.GroupBean;
import com.hubloy.sms.beans.contact.GroupListBean;
import com.hubloy.sms.constants.FileStatus;
import com.hubloy.sms.constants.ResponseStatus;
import com.hubloy.sms.helpers.DateHelpers;
import com.hubloy.sms.helpers.DirectoryHelpers;
import com.hubloy.sms.helpers.FileUtils;
import com.hubloy.sms.helpers.PaginationHelpers;
import com.hubloy.sms.helpers.StringHelpers;
import com.hubloy.sms.orm.account.Account;
import com.hubloy.sms.orm.account.AccountUser;
import com.hubloy.sms.orm.account.User;
import com.hubloy.sms.orm.contacts.Contact;
import com.hubloy.sms.orm.contacts.ContactGroup;
import com.hubloy.sms.orm.documents.ContactFile;
import com.hubloy.sms.repository.account.AccountUserRepository;
import com.hubloy.sms.repository.contacts.ContactGroupRepository;
import com.hubloy.sms.repository.contacts.ContactRespository;
import com.hubloy.sms.repository.documents.ContactFileRepository;

@Service
public class ContactService {

	@Value("${app.ctx}")
	private String path;

	private final AccountUserRepository accountUserRepository;

	private final ContactRespository contactRespository;

	private final ContactGroupRepository contactGroupRepository;

	private final ContactFileRepository contactFileRepository;

	@Autowired
	public ContactService(AccountUserRepository accountUserRepository, ContactRespository contactRespository,
			ContactGroupRepository contactGroupRepository, ContactFileRepository contactFileRepository) {
		super();
		this.accountUserRepository = accountUserRepository;
		this.contactRespository = contactRespository;
		this.contactGroupRepository = contactGroupRepository;
		this.contactFileRepository = contactFileRepository;
	}

	/**
	 * List Contacts
	 * 
	 * @param user
	 * @param page
	 * @return
	 */
	public ContactListBean listContacts(User user, int page) {
		ContactListBean bean = new ContactListBean();
		AccountUser accountUser = accountUserRepository.findByUser(user);
		if (accountUser != null) {
			Account account = accountUser.getAccount();
			bean.setTotal(contactRespository.countByAccount(account));
			List<ContactBean> rows = new ArrayList<ContactBean>();
			List<Contact> contacts = contactRespository.findByAccount(account,
					PaginationHelpers.paginationIdDesc(page, 10));
			for (Contact contact : contacts) {
				rows.add(contactToBean(contact));
			}
			bean.setRows(rows);
		}
		return bean;
	}

	/**
	 * Get Contacts by group
	 * 
	 * @param user
	 * @param group
	 * @param page
	 * @return
	 */
	public ContactListBean listGroupContacts(User user, String group, int page) {
		ContactListBean bean = new ContactListBean();
		AccountUser accountUser = accountUserRepository.findByUser(user);
		if (accountUser != null) {
			ContactGroup contactGroup = contactGroupRepository.findByItemId(group);
			if (contactGroup != null) {
				Account account = accountUser.getAccount();
				bean.setTotal(contactRespository.countByAccountAndContactGroup(account, contactGroup));
				List<ContactBean> rows = new ArrayList<ContactBean>();
				List<Contact> contacts = contactRespository.findByAccountAndContactGroup(account, contactGroup,
						PaginationHelpers.paginationIdDesc(page, 10));
				for (Contact contact : contacts) {
					rows.add(contactToBean(contact));
				}
				bean.setRows(rows);
			}

		}
		return bean;
	}

	/**
	 * List groups
	 * 
	 * @param user
	 * @param page
	 * @return
	 */
	public GroupListBean listGroups(User user, int page) {
		GroupListBean bean = new GroupListBean();
		AccountUser accountUser = accountUserRepository.findByUser(user);
		if (accountUser != null) {
			Account account = accountUser.getAccount();
			List<ContactGroup> groups = contactGroupRepository.findByAccount(account,
					PaginationHelpers.paginationIdDesc(page, 10));
			List<GroupBean> rows = new ArrayList<GroupBean>();
			for (ContactGroup group : groups) {
				rows.add(groupToBean(group));
			}
			bean.setTotal(contactGroupRepository.countByAccount(account));
			bean.setRows(rows);
		}
		return bean;
	}

	/**
	 * List all groups
	 * 
	 * @param user
	 * @return
	 */
	public List<GroupBean> listGroups(User user) {
		List<GroupBean> list = new ArrayList<GroupBean>();
		AccountUser accountUser = accountUserRepository.findByUser(user);
		if (accountUser != null) {
			Account account = accountUser.getAccount();
			List<ContactGroup> groups = contactGroupRepository.findByAccount(account);
			for (ContactGroup group : groups) {
				list.add(groupToBean(group));
			}
		}
		return list;
	}

	/**
	 * Save Contact
	 * 
	 * @param user
	 * @param name
	 * @param phone
	 * @param group
	 * @return
	 */
	public GenericMessage saveContact(User user, String name, String phone, String group) {
		AccountUser accountUser = accountUserRepository.findByUser(user);
		if (accountUser != null) {
			Account account = accountUser.getAccount();
			Contact contact = new Contact();
			contact.setName(name);
			if (!phone.startsWith("+")) {
				phone = "+" + phone;
			}
			contact.setPhone(phone);
			if (group != null && !group.isEmpty()) {
				ContactGroup contactGroup = contactGroupRepository.findByItemId(group);
				if (contactGroup != null) {
					contact.setContactGroup(contactGroup);
				}
			}
			contact.setAccount(account);
			contactRespository.saveAndFlush(contact);
			return new GenericMessage(ResponseStatus.SUCCESS, "Contact Saved");
		} else {
			return new GenericMessage(ResponseStatus.ERROR, "User has no account");
		}
	}

	/**
	 * Save Group
	 * 
	 * @param user
	 * @param name
	 * @return
	 */
	public GenericMessage saveGroup(User user, String name) {
		AccountUser accountUser = accountUserRepository.findByUser(user);
		if (accountUser != null) {
			Account account = accountUser.getAccount();
			ContactGroup contactGroup = new ContactGroup();
			contactGroup.setAccount(account);
			contactGroup.setName(name);
			contactGroupRepository.saveAndFlush(contactGroup);
			return new GenericMessage(ResponseStatus.SUCCESS, "Contact Group Saved");
		} else {
			return new GenericMessage(ResponseStatus.ERROR, "User has no account");
		}
	}

	/**
	 * Delete contact
	 * 
	 * @param id
	 * @return
	 */
	public GenericMessage deleteContact(String id) {
		Contact contact = contactRespository.findByItemId(id);
		if (contact != null) {
			contactRespository.delete(contact);
			return new GenericMessage(ResponseStatus.SUCCESS, "Contact deleted");
		} else {
			return new GenericMessage(ResponseStatus.ERROR, "Contact does not exist");
		}
	}

	/**
	 * Delete contact group
	 * 
	 * @param id
	 * @return
	 */
	public GenericMessage deleteContactGroup(String id) {
		ContactGroup contactGroup = contactGroupRepository.findByItemId(id);
		if (contactGroup != null) {
			List<Contact> contacts = contactRespository.findByContactGroup(contactGroup);
			contactRespository.deleteInBatch(contacts);
			contactGroupRepository.delete(contactGroup);
			return new GenericMessage(ResponseStatus.SUCCESS, "Contact group deleted");
		} else {
			return new GenericMessage(ResponseStatus.ERROR, "Contact group does not exist");
		}
	}

	/**
	 * Get Group
	 * 
	 * @param id
	 * @return
	 */
	public GroupBean getGroup(String id) {
		GroupBean bean = new GroupBean();
		ContactGroup contactGroup = contactGroupRepository.findByItemId(id);
		if (contactGroup != null) {
			bean = groupToBean(contactGroup);
		} else {
			bean.setStatus(ResponseStatus.ERROR);
		}
		return bean;
	}

	/**
	 * Upload Contacts
	 * 
	 * @param group
	 * @param user
	 * @param request
	 * @return
	 */
	@Async("asyncExecutor")
	@Transactional
	public CompletableFuture<GenericMessage> uploadCSVContacts(String group, User user, MultipartFile file) {
		AccountUser accountUser = accountUserRepository.findByUser(user);
		if (accountUser != null) {
			Account account = accountUser.getAccount();
			ContactGroup contactGroup = null;
			DirectoryHelpers.setDirectories(path);
			String path = DirectoryHelpers.getContactDirectory();
			JSONObject jsonObject = FileUtils.uploadCSV(path, file);
			if (jsonObject.length() > 0) {
				String filePath = jsonObject.getString("path");
				ContactFile contactFile = new ContactFile();
				contactFile.setAccount(accountUser.getAccount());
				contactFile.setUser(user);
				contactFile.setFileStatus(FileStatus.Pending);
				contactFile.setPath(filePath);
				contactFile.setProcessed(false);
				if (group != null && !group.isEmpty()) {
					contactGroup = contactGroupRepository.findByItemId(group);
					if (contactGroup != null) {
						contactFile.setContactGroup(contactGroup);
					}
				}
				
				ContactFile cfile = contactFileRepository.saveAndFlush(contactFile);
				List<Contact> contacts = new ArrayList<Contact>();
				Contact contact = null;
				File cFilePath = new File(filePath);
				try {
					LineIterator it = org.apache.commons.io.FileUtils.lineIterator(cFilePath, "UTF-8");
					String line = null;
					String[] parts = null;
					String phone = null;
				    while (it.hasNext()) {
				        line = it.nextLine();
				        parts = StringHelpers.splitString(line);
				        if(parts.length > 1) {
				        	contact = new Contact();
				        	contact.setAccount(account);
				        	if (contactGroup != null) {
				        		contact.setContactGroup(contactGroup);
				        	}
				        	phone = parts[1];
				        	if (!phone.startsWith("+")) {
								phone = "+" + phone;
							}
				        	contact.setName(parts[0]);
				        	contact.setPhone(phone);
				        	contacts.add(contact);
				        }
				    }
				    if(!contacts.isEmpty()) {
				    	contactRespository.saveAll(contacts);
				    	contactFileRepository.delete(cfile);
				    	cFilePath.delete();
				    	return CompletableFuture.completedFuture(new GenericMessage(ResponseStatus.SUCCESS, contacts.size() + " contacts saved"));
				    } else {
				    	cfile.setProcessed(true);
				    	cfile.setFileStatus(FileStatus.Error);
				    	contactFileRepository.saveAndFlush(cfile);
				    	return CompletableFuture.completedFuture(new GenericMessage(ResponseStatus.ERROR, "No contacts to save. Please check the file format"));
				    }
				    
				} catch (IOException e) {
					return CompletableFuture.completedFuture(new GenericMessage(ResponseStatus.ERROR, "Error processing file " + e.getMessage()));
				}
			} else {
				return CompletableFuture.completedFuture(new GenericMessage(ResponseStatus.ERROR, "Error uploading file"));
			}

		} else {
			return CompletableFuture.completedFuture(new GenericMessage(ResponseStatus.ERROR, "User has no account"));
		}
	}
	
	
	@Async("asyncExecutor")
	@Transactional
	public CompletableFuture<GenericMessage> uploadExcelContacts(String group, User user, MultipartFile file) {
		AccountUser accountUser = accountUserRepository.findByUser(user);
		if (accountUser != null) {
			Account account = accountUser.getAccount();
			ContactGroup contactGroup = null;
			DirectoryHelpers.setDirectories(path);
			String path = DirectoryHelpers.getContactDirectory();
			JSONObject jsonObject = FileUtils.uploadExcel(path, file);
			if (jsonObject.length() > 0) {
				String filePath = jsonObject.getString("path");
				ContactFile contactFile = new ContactFile();
				contactFile.setAccount(accountUser.getAccount());
				contactFile.setUser(user);
				contactFile.setFileStatus(FileStatus.Pending);
				contactFile.setPath(filePath);
				contactFile.setProcessed(false);
				if (group != null && !group.isEmpty()) {
					contactGroup = contactGroupRepository.findByItemId(group);
					if (contactGroup != null) {
						contactFile.setContactGroup(contactGroup);
					}
				}
				
				ContactFile cfile = contactFileRepository.saveAndFlush(contactFile);
				List<Contact> contacts = new ArrayList<Contact>();
				Contact contact = null;
				File cFilePath = new File(filePath);
				FileInputStream fileInputStream = null;
				HSSFWorkbook workbook = null;
				try {
					fileInputStream = new FileInputStream(cFilePath);
					workbook = new HSSFWorkbook(fileInputStream);
					HSSFSheet sheet = workbook.getSheetAt(0);
					Cell name = null;
					Cell phone = null;
					String phonenumber = null;
					DataFormatter formatter = new DataFormatter();
					for(Row row: sheet) {
						name = row.getCell(0);
						phone = row.getCell(1);
						if (name != null && phone != null) {
							contact = new Contact();
				        	contact.setAccount(account);
				        	if (contactGroup != null) {
				        		contact.setContactGroup(contactGroup);
				        	}
				        	if( phone.getCellType() == CellType.NUMERIC) {
				        		phonenumber = formatter.formatCellValue(phone)+"";
				        	} else {
				        		phonenumber = phone.getStringCellValue();
				        	}
				        	
				        	if (!phonenumber.startsWith("+")) {
				        		phonenumber = "+" + phonenumber;
							}
				        	contact.setName(name.getStringCellValue());
				        	contact.setPhone(phonenumber);
				        	contacts.add(contact);
						}
					}
					workbook.close();
					fileInputStream.close();
					workbook = null;
					fileInputStream = null;
				    if(!contacts.isEmpty()) {
				    	contactRespository.saveAll(contacts);
				    	contactFileRepository.delete(cfile);
				    	cFilePath.delete();
				    	return CompletableFuture.completedFuture(new GenericMessage(ResponseStatus.SUCCESS, contacts.size() + " contacts saved"));
				    } else {
				    	cfile.setProcessed(true);
				    	cfile.setFileStatus(FileStatus.Error);
				    	contactFileRepository.saveAndFlush(cfile);
				    	return CompletableFuture.completedFuture(new GenericMessage(ResponseStatus.ERROR, "No contacts to save. Please check the file format"));
				    }
				    
				} catch (IOException e) {
					return CompletableFuture.completedFuture(new GenericMessage(ResponseStatus.ERROR, "Error processing file " + e.getMessage()));
				} finally {
					if (workbook != null) {
						try {
							workbook.close();
						} catch (IOException e) {
						}
					}
					if (fileInputStream != null) {
						try {
							fileInputStream.close();
						} catch (IOException e) {
						}
					}
				}
			} else {
				return CompletableFuture.completedFuture(new GenericMessage(ResponseStatus.ERROR, "Error uploading file"));
			}

		} else {
			return CompletableFuture.completedFuture(new GenericMessage(ResponseStatus.ERROR, "User has no account"));
		}
	}

	/**
	 * Contact to bean
	 * 
	 * @param contact
	 * @return
	 */
	private ContactBean contactToBean(Contact contact) {
		ContactBean bean = new ContactBean();
		bean.setId(contact.getItemId());
		bean.setCreatedAt(DateHelpers.formatToSQLDateString(contact.getDateCreated()));
		if (contact.getContactGroup() != null) {
			bean.setGroup(contact.getContactGroup().getName());
		} else {
			bean.setGroup("None");
		}
		bean.setName(contact.getName());
		bean.setPhone(contact.getPhone());
		return bean;
	}

	/**
	 * Group to bean
	 * 
	 * @param group
	 * @return
	 */
	private GroupBean groupToBean(ContactGroup group) {
		GroupBean bean = new GroupBean();
		bean.setId(group.getItemId());
		bean.setName(group.getName());
		bean.setContacts(contactRespository.countByContactGroup(group));
		return bean;
	}
}
