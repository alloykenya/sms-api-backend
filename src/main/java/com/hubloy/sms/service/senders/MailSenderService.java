package com.hubloy.sms.service.senders;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hubloy.sms.base.BaseHelper;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;

@Service
public class MailSenderService {

	@Autowired
	private SendGrid sendGrid;

	
	/**
	 * Send Mail
	 * 
	 * @param from
	 * @param to
	 * @param subject
	 * @param body
	 * @return
	 */
	public boolean sendMail(String from, String to, String subject, String body) {
		Response response = sendEmail(from, to, subject, new Content("text/html", body));
		if(response.getStatusCode() == 202) {
			return true;
		}
		return false;
	}

	/**
	 * Mail helper
	 * 
	 * @param from
	 * @param to
	 * @param subject
	 * @param content
	 * @return
	 */
	private Response sendEmail(String from, String to, String subject, Content content) {
        Mail mail = new Mail(new Email(from), subject, new Email(to), content);
        mail.setReplyTo(new Email("no-reply@mobtechsolutions.com"));
        Request request = new Request();
        Response response = null;
        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            response = sendGrid.api(request);
        } catch (IOException e) {
           	BaseHelper.log().error(e.getMessage(), e);
        }
        return response;
    }
}
