package com.hubloy.sms.service.senders;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.hubloy.sms.base.GenericMessage;
import com.hubloy.sms.beans.senders.SMSSenderBean;
import com.hubloy.sms.beans.senders.SMSSenderListBean;
import com.hubloy.sms.constants.ResponseStatus;
import com.hubloy.sms.helpers.PaginationHelpers;
import com.hubloy.sms.orm.account.Account;
import com.hubloy.sms.orm.senders.AccountSender;
import com.hubloy.sms.orm.senders.SMSSender;
import com.hubloy.sms.orm.settings.SMSAPISettings;
import com.hubloy.sms.repository.account.AccountRepository;
import com.hubloy.sms.repository.senders.AccountSenderRepository;
import com.hubloy.sms.repository.senders.SMSSenderRepository;
import com.hubloy.sms.repository.settings.SMSAPISettingsRepository;

@Service
public class SMSSenderService {

	private final SMSSenderRepository smsSenderRepository;

	private final AccountSenderRepository accountSenderRepository;

	private final AccountRepository accountRepository;

	private final SMSAPISettingsRepository smsapiSettingsRepository;

	@Autowired
	public SMSSenderService(SMSSenderRepository smsSenderRepository, AccountSenderRepository accountSenderRepository,
			AccountRepository accountRepository, SMSAPISettingsRepository smsapiSettingsRepository) {
		super();
		this.smsSenderRepository = smsSenderRepository;
		this.accountSenderRepository = accountSenderRepository;
		this.accountRepository = accountRepository;
		this.smsapiSettingsRepository = smsapiSettingsRepository;
	}

	/**
	 * List Senders
	 * 
	 * @param page
	 * @return
	 */
	public SMSSenderListBean listSenders(int page) {
		SMSSenderListBean bean = new SMSSenderListBean();
		bean.setTotal(smsSenderRepository.count());
		List<SMSSenderBean> list = new ArrayList<SMSSenderBean>();
		Page<SMSSender> senders = smsSenderRepository.findAll(PaginationHelpers.paginationIdDesc(page, 10));
		for (SMSSender sender : senders) {
			list.add(senderToBean(sender, ""));
		}
		bean.setRows(list);
		return bean;
	}

	/**
	 * List Account Senders
	 * 
	 * @param accountId
	 * @param page
	 * @return
	 */
	public SMSSenderListBean listAccountSenders(String accountId, int page) {
		SMSSenderListBean bean = new SMSSenderListBean();
		Account account = accountRepository.findByItemId(accountId);
		if (account != null) {
			bean.setTotal(accountSenderRepository.countByAccount(account));
			List<SMSSenderBean> list = new ArrayList<SMSSenderBean>();
			List<AccountSender> senders = accountSenderRepository.findByAccount(account,
					PaginationHelpers.paginationIdDesc(page, 10));
			for (AccountSender sender : senders) {
				list.add(senderToBean(sender.getSmsSender(), sender.getItemId()));
			}
			bean.setRows(list);
		}
		return bean;
	}
	
	/**
	 * List all senders
	 * 
	 * @param accountId
	 * @return
	 */
	public List<SMSSenderBean> listAllSenders() {
		List<SMSSenderBean> list = new ArrayList<SMSSenderBean>();
		List<SMSSender> senders = smsSenderRepository.findAll();
		for (SMSSender sender : senders) {
			list.add(senderToBean(sender, ""));
		}
		return list;
	}
	
	
	public List<SMSSenderBean> listAllAccountSenders(String accountId) {
		List<SMSSenderBean> list = new ArrayList<SMSSenderBean>();
		Account account = accountRepository.findByItemId(accountId);
		if (account != null) {
			List<AccountSender> senders = accountSenderRepository.findByAccount(account);
			for (AccountSender sender : senders) {
				list.add(senderToBean(sender.getSmsSender(), sender.getItemId()));
			}
		}
		return list;
	}

	/**
	 * Save Sender
	 * 
	 * @param sender
	 * @return
	 */
	public GenericMessage saveSender(String sender) {
		SMSSender smsSender = smsSenderRepository.findBySender(sender);
		if (smsSender == null) {
			List<SMSAPISettings> list = smsapiSettingsRepository.findAll();
			if (!list.isEmpty()) {
				smsSender = new SMSSender();
				SMSAPISettings settings = list.get(0);
				smsSender.setSettings(settings);
				smsSender.setSender(sender);
				smsSenderRepository.saveAndFlush(smsSender);
				return new GenericMessage(ResponseStatus.SUCCESS, "Sender Saved");
			} else {
				return new GenericMessage(ResponseStatus.ERROR, "No SMS Account saved");
			}

		} else {
			return new GenericMessage(ResponseStatus.ERROR, "Sender Exists");
		}
	}
	
	/**
	 * 
	 * <p>
	 * Remove sender
	 * </p>
	 * @param senderId
	 * @return
	 */
	public GenericMessage removeSender(String senderId) {
		SMSSender sender = smsSenderRepository.findByItemId(senderId);
		if (sender != null) {
			List<AccountSender> accountSenders = accountSenderRepository.findBySmsSender(sender);
			for(AccountSender accountSender : accountSenders) {
				accountSenderRepository.delete(accountSender);
			}
			smsSenderRepository.delete(sender);
			return new GenericMessage(ResponseStatus.SUCCESS, "Sender removed");
		} else {
			return new GenericMessage(ResponseStatus.ERROR, "Sender does not exist");
		}
	}

	/**
	 * Add sender to account
	 * 
	 * @param senderId
	 * @param accountId
	 * @return
	 */
	public GenericMessage addSenderToAccount(String senderId, String accountId) {
		Account account = accountRepository.findByItemId(accountId);
		if (account != null) {
			SMSSender sender = smsSenderRepository.findByItemId(senderId);
			if (sender != null) {
				AccountSender accountSender = accountSenderRepository.findBySmsSenderAndAccount(sender, account);
				if (accountSender == null) {
					accountSender = new AccountSender();
					accountSender.setAccount(account);
					accountSender.setSmsSender(sender);
					accountSenderRepository.saveAndFlush(accountSender);
					return new GenericMessage(ResponseStatus.SUCCESS, "Sender assigned to account");
				} else {
					return new GenericMessage(ResponseStatus.ERROR, "Sender is already assigned to account");
				}
			} else {
				return new GenericMessage(ResponseStatus.ERROR, "Sender does not exist");
			}
		} else {
			return new GenericMessage(ResponseStatus.ERROR, "Account does not exist");
		}
	}

	/**
	 * Remove sender
	 * 
	 * @param id
	 * @return
	 */
	public GenericMessage removeSenderFromAccount(String id) {
		AccountSender accountSender = accountSenderRepository.findByItemId(id);
		if (accountSender != null) {
			accountSenderRepository.delete(accountSender);
			return new GenericMessage(ResponseStatus.SUCCESS, "Removed");
		} else {
			return new GenericMessage(ResponseStatus.ERROR, "Account sender does not exist");
		}
	}

	/**
	 * Sender to bean
	 * 
	 * @param sender
	 * @return
	 */
	private SMSSenderBean senderToBean(SMSSender sender, String accountSenderid) {
		SMSSenderBean bean = new SMSSenderBean();
		bean.setId(sender.getItemId());
		bean.setSender(sender.getSender());
		bean.setSettingId(sender.getSettings().getItemId());
		bean.setAccounts(accountSenderRepository.countBySmsSender(sender));
		bean.setAccountSenderid(accountSenderid);
		return bean;
	}
}
