package com.hubloy.sms.helpers;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.hubloy.sms.base.BaseHelper;

/**
 * 
 * StringHelpers
 * 
 * @author Paul Kevin
 *
 * @version enter version, 26 Feb 2018
 *
 * @since jdk 1.8
 */
public class StringHelpers extends BaseHelper {

	/**
	 * Email validation
	 */
	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
			Pattern.CASE_INSENSITIVE);

	/**
	 * String to array
	 * 
	 * @param str
	 * @return
	 */
	public static List<String> stringToArray(String str) {
		String[] items = splitString(str);
		return Arrays.asList(items);
	}

	/**
	 * String to String list
	 * 
	 * @param str
	 * @return
	 */
	public static String[] splitString(String str) {
		return str.split(",");
	}

	/**
	 * Generate url slugs
	 * 
	 * @param name
	 * @return
	 */
	public static String generateSlug(String name) {
		return name.replaceAll(" ", "-").toLowerCase();
	}

	/**
	 * Validate email
	 * 
	 * @param emailStr
	 * @return
	 */
	public static boolean validateEmail(String emailStr) {
		Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
		return matcher.find();
	}

	public static String removePrefix(String string, String prefix) {
		return string.replace(prefix, "");
	}

	/**
	 * 
	 * <p>
	 * Get message parts
	 * </p>
	 * 
	 * @param message
	 * @return
	 */
	public static int countSMSMessages(String input, int size) {
		List<String> ret = new ArrayList<String>((input.length() + size - 1) / size);

	    for (int start = 0; start < input.length(); start += size) {
	        ret.add(input.substring(start, Math.min(input.length(), start + size)));
	    }
	    return ret.size();
	}

	public static String formatCurrency(float number) {
		NumberFormat formatter = NumberFormat.getCurrencyInstance();
		DecimalFormatSymbols decimalFormatSymbols = ((DecimalFormat) formatter).getDecimalFormatSymbols();
		decimalFormatSymbols.setCurrencySymbol("");
		((DecimalFormat) formatter).setDecimalFormatSymbols(decimalFormatSymbols);
		return formatter.format(number);
	}

	public static void main(String[] args) {
		System.out.println(countSMSMessages("It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters it has a more-or-less normal distribution of letters it has a more-or-less normal distribution of letters it has a more-or-less normal distribution of letters", 160));
	}
}
