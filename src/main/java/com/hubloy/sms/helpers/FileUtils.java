package com.hubloy.sms.helpers;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Random;
import java.util.UUID;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.springframework.web.multipart.MultipartFile;

public class FileUtils {

	protected static final Log LOGGER = LogFactory.getLog(FileUtils.class);

	/* remove leading whitespace */
	public static String ltrim(String source) {
		return source.replaceAll("^\\s+", "");
	}

	/* remove trailing whitespace */
	public static String rtrim(String source) {
		return source.replaceAll("\\s+$", "");
	}

	/* replace multiple whitespaces between words with single blank */
	public static String itrim(String source) {
		return source.replaceAll("\\b\\s{2,}\\b", " ");
	}

	/* remove all superfluous whitespaces in source string */
	public static String trim(String source) {
		return itrim(ltrim(rtrim(source)));
	}

	public static String lrtrim(String source) {
		return ltrim(rtrim(source));
	}

	/**
	 * 
	 * <p>
	 * Delete file
	 * </p>
	 * 
	 * @param fileLocation
	 */
	public static void deleteFile(String fileLocation) {
		File file = new File(fileLocation);
		if (file.exists()) {
			if (!file.delete()) {
				file.deleteOnExit();
			}
		}
	}

	/**
	 * 
	 * <p>
	 * Delete file
	 * </p>
	 * 
	 * @param file
	 */
	public static void deleteFile(File file) {
		if (file.exists()) {
			if (!file.delete()) {
				file.deleteOnExit();
			}
		}
	}

	/**
	 * 
	 * <p>
	 * Create directory
	 * </p>
	 * 
	 * @param dir
	 */
	public static void createdir(String dir) {
		File file = new File(dir);
		if (!file.exists())
			file.mkdirs();

		file = new File(dir);
		if (!file.exists())
			file.mkdirs();
		file.setWritable(true);

	}

	/**
	 * 
	 * <p>
	 * Create file
	 * </p>
	 * 
	 * @param filename
	 */
	public static void createfile(String filename) {
		File file = new File(filename);
		if (!file.exists())
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}

		file = new File(filename);
		if (!file.exists())
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}

	}

	/**
	 * 
	 * <p>
	 * Create or append file
	 * </p>
	 * 
	 * @param f
	 * @param text
	 * @throws IOException
	 */
	public static void createOrAppendFile(File f, String text)
			throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(f, true));
		bw.write(text);
		bw.newLine();
		bw.flush();
		bw.close();
	}

	/**
	 * 
	 * <p>
	 * Create file but dont append
	 * </p>
	 * 
	 * @param f
	 * @param text
	 * @throws IOException
	 */
	public static void createFile(File f, String text) throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(f, false));
		bw.write(text);
		bw.flush();
		bw.close();
	}

	public static void createFile(String fileName, String content)
			throws IOException {
		File f = new File(fileName);
		createOrAppendFile(f, content);
	}

	/**
	 * 
	 * <p>
	 * Move file to a destination
	 * </p>
	 * 
	 * @param file
	 * @param destination
	 * @throws Exception
	 */
	public static void moveFile(File file, String destination) throws Exception {
		FileOutputStream fos = null;
		InputStream is = null;
		is = new FileInputStream(file);
		File dest = new File(destination, file.getName());
		fos = new FileOutputStream(dest);
		IOUtils.copy(is, fos);
		file.delete();
	}

	/**
	 * Read text file
	 * 
	 * @return
	 */
	public static String readFile(String filename) {
		FileInputStream fstream;
		StringBuilder builder = new StringBuilder();
		try {
			fstream = new FileInputStream(filename);
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			// Read File Line By Line
			while ((strLine = br.readLine()) != null) {
				builder.append(strLine);
			}
			in.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return builder.toString();
	}

	public static void createDocTextFile(String fileName) throws IOException {
		// path to create
		FileWriter ryt = new FileWriter(fileName);
		BufferedWriter out = new BufferedWriter(ryt);
		out.write("");
		out.close();
	}

	public static boolean deleteDir(File dir) {
		if (dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					System.out
							.println("Cannot delete " + dir.getAbsolutePath());
					return false;
				}
			}
		}

		// The directory is now empty so delete it
		return dir.delete();
	}

	public static boolean compareMimetypes(String file, String mimetype) {
		boolean istype = false;
		if (file.equals(mimetype)) {
			istype = true;
		}
		return istype;
	}

	/*
	 * This Method Handles streaming Binary data <p> @param String urlstr ex:
	 * http;//localhost/test.pdf etc. @param String format ex: pdf or audio_wav
	 * or msdocuments etc. @param ServletOutputStream outstr @param
	 * HttpServletResponse resp
	 */
	public static void streamBinaryData(String urlstr, String format,
			HttpServletResponse resp, HttpServletRequest request) {
		String ErrorStr = null;

		try {
			ServletOutputStream outstr = resp.getOutputStream();
			// find the right mime type and set it as contenttype
			BufferedInputStream bis = null;
			BufferedOutputStream bos = null;
			try {
				URL url = new URL(request.getScheme(), request.getServerName(),
						request.getServerPort(), urlstr);
				URLConnection urlc = url.openConnection();
				int length = urlc.getContentLength();
				resp.setContentType(format);
				resp.setContentLength(length);
				// Use Buffered Stream for reading/writing.
				InputStream in = urlc.getInputStream();
				bis = new BufferedInputStream(in);
				bos = new BufferedOutputStream(outstr);
				byte[] buff = new byte[length];
				int bytesRead;
				// Simple read/write loop.
				while (-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
					bos.write(buff, 0, bytesRead);
				}
			} catch (Exception e) {
				LOGGER.error(e.getMessage(), e);
				ErrorStr = "Error Streaming the Data";
				outstr.print(ErrorStr);
			} finally {
				if (bis != null) {
					bis.close();
				}
				if (bos != null) {
					bos.close();
				}
				if (outstr != null) {
					outstr.flush();
					outstr.close();
				}
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
	}


	/*
	 * This Method Handles streaming Character data <p> @param String urlstr ex:
	 * http;//localhost/test.pdf etc. @param String format ex: xml or html etc.
	 * 
	 * @param PrintWriter outstr @param HttpServletResponse resp
	 */
	public static void streamCharacterData(String urlstr, String format,
			PrintWriter outstr, HttpServletResponse resp) {
		String ErrorStr = null;

		// find the right mime type and set it as contenttype
		InputStream in = null;
		try {
			URL url = new URL(urlstr);
			URLConnection urlc = url.openConnection();
			int length = urlc.getContentLength();
			in = urlc.getInputStream();
			resp.setContentType(format);
			resp.setContentLength(length);
			int ch;
			while ((ch = in.read()) != -1) {
				outstr.print((char) ch);
			}
		} catch (Exception e) {
			ErrorStr = "Error Streaming the Data";
			outstr.print(ErrorStr);
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {

				}
			}
			if (outstr != null) {
				outstr.flush();
				outstr.close();
			}
		}

	}

	public static void resizeImage(File originalFile, File destinationFile,
			int width, int height) throws IOException {
		BufferedImage bufferedImage = ImageIO.read(originalFile);
		int calcHeight = height > 0 ? height : (width
				* bufferedImage.getHeight() / bufferedImage.getWidth());

		BufferedImage scaledBI = new BufferedImage(width, calcHeight,
				BufferedImage.TYPE_INT_RGB);
		Graphics2D g = scaledBI.createGraphics();
		g.setComposite(AlphaComposite.Src);
		g.drawImage(bufferedImage, 0, 0, width, calcHeight, null);
		g.dispose();
		ImageIO.write(scaledBI, "JPG", destinationFile);
	}

	/**
	 * 
	 * <p>
	 * Rename file
	 * </p>
	 * 
	 * @param filename
	 * @return
	 */
	public static String rename(String filename) {

		// Get the extension if the file has one
		String fileExt = "";
		Random random = new Random();
		int i = -1;
		if ((i = filename.indexOf(".")) != -1) {
			fileExt = filename.substring(i);
			filename = filename.substring(0, i);
		}

		// add the timestamp
		int randomNum = random.nextInt(100000);
		filename = String.valueOf((new Date().getTime()) + randomNum);

		// piece together the filename
		filename = filename + fileExt;

		String newName = filename;

		return newName;
	}

	/**
	 * 
	 * <p>
	 * Move file from one destination to another
	 * </p>
	 * 
	 * @param source
	 * @param destination
	 * @return
	 */
	public static boolean moveFile(String source, String destination) {
		boolean moved = false;
		File f1 = new File(source);
		File f2 = new File(destination);

		InputStream in = null;
		OutputStream out = null;

		try {
			in = new FileInputStream(f1);
			out = new FileOutputStream(f2, true);
			IOUtils.copy(in, out);
			moved = true;
		} catch (FileNotFoundException e) {
			LOGGER.error(e.getMessage(), e);
		} catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
		}
		return moved;
	}

	/**
	 * 
	 * <p>
	 * Remove file extension
	 * </p>
	 * 
	 * @param filename
	 * @return
	 */
	public static String removeExtension(String filename) {
		return FilenameUtils.removeExtension(filename);
	}

	/**
	 * 
	 * <p>
	 * Search file by name
	 * </p>
	 * 
	 * @param name
	 * @param dir
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static String searchFile(String name, String dir) {
		String filepath = null;
		File root = new File(dir);
		try {
			boolean recursive = true;

			Collection files = org.apache.commons.io.FileUtils.listFiles(root,
					null, recursive);
			for (Iterator<File> iterator = files.iterator(); iterator.hasNext();) {
				File file = iterator.next();
				if (removeExtension(file.getName()).equals(name))
					filepath = file.getAbsolutePath();
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
		return filepath;
	}
	
	public static JSONObject uploadCSV(String destination, MultipartFile file) {

		JSONObject upload = new JSONObject();
		BufferedOutputStream buffStream = null;
		String newFileName = null;
		String filename = UUID.randomUUID().toString() + ".csv";
		try {
			if (file.getContentType().equalsIgnoreCase("text/csv")) {
				filename = file.getOriginalFilename();
				byte[] bytes = file.getBytes();

				newFileName = FileUtils.rename(filename);
				buffStream = new BufferedOutputStream(new FileOutputStream(
						new File(destination, newFileName)));
				buffStream.write(bytes);
				upload.put("file", newFileName);
				upload.put("path", destination +  DirectoryHelpers.fileSeperator + newFileName);
			}else{
				LOGGER.info("Invalid File Type ");
			}
		} catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
		} finally {
			try {
				if (buffStream != null)
					buffStream.close();
			} catch (IOException e) {
				LOGGER.error(e.getMessage(), e);
			}
		}

		return upload;
	}
	
	
	public static JSONObject uploadExcel(String destination, MultipartFile file) {

		JSONObject upload = new JSONObject();
		BufferedOutputStream buffStream = null;
		String newFileName = null;
		String filename = UUID.randomUUID().toString() + ".xls";
		try {
			if (file.getContentType().equalsIgnoreCase("application/vnd.ms-excel")) {
				filename = file.getOriginalFilename();
				byte[] bytes = file.getBytes();

				newFileName = FileUtils.rename(filename);
				buffStream = new BufferedOutputStream(new FileOutputStream(
						new File(destination, newFileName)));
				buffStream.write(bytes);
				upload.put("file", newFileName);
				upload.put("path", destination +  DirectoryHelpers.fileSeperator + newFileName);
			}else{
				LOGGER.info("Invalid File Type ");
			}
		} catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
		} finally {
			try {
				if (buffStream != null)
					buffStream.close();
			} catch (IOException e) {
				LOGGER.error(e.getMessage(), e);
			}
		}

		return upload;
	}
}