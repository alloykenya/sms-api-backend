package com.hubloy.sms.helpers;

import java.io.File;

public class DirectoryHelpers {

	public static final String fileSeperator = File.separator;

	private static String rootDirectory = null;

	private static String systemDirectory = null;

	private static String contactDirectory = null;
	private static String messageDirectory = null;

	public static void setDirectories(String cctx) {
		rootDirectory = cctx;
		systemDirectory = rootDirectory + fileSeperator + "system";
		contactDirectory = systemDirectory + fileSeperator + "contacts";
		messageDirectory = systemDirectory + fileSeperator + "messages";
	}

	public static String getRootDirectory() {
		return rootDirectory;
	}

	public static void setRootDirectory(String rootDirectory) {
		DirectoryHelpers.rootDirectory = rootDirectory;
	}

	public static String getSystemDirectory() {
		return systemDirectory;
	}

	public static void setSystemDirectory(String systemDirectory) {
		DirectoryHelpers.systemDirectory = systemDirectory;
	}

	public static String getContactDirectory() {
		return contactDirectory;
	}

	public static void setContactDirectory(String contactDirectory) {
		DirectoryHelpers.contactDirectory = contactDirectory;
	}

	public static String getMessageDirectory() {
		return messageDirectory;
	}

	public static void setMessageDirectory(String messageDirectory) {
		DirectoryHelpers.messageDirectory = messageDirectory;
	}
}
