package com.hubloy.sms.config;

import java.util.concurrent.Executor;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.sendgrid.SendGrid;

@Configuration
@EnableAutoConfiguration
@ComponentScan("com.hubloy.sms")
@EnableJpaRepositories(basePackages = { "com.hubloy.sms.repository" })
@EnableTransactionManagement
@EnableScheduling
@EnableAsync
public class RepositoryConfiguration {
	
	@Value("${sendgrid.api}")
	private String apiKey;

	@Bean(name = "asyncExecutor")
	public Executor asyncExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(10);
		executor.setMaxPoolSize(10);
		executor.setQueueCapacity(20);
		executor.setThreadNamePrefix("AsynchThread-");
		executor.initialize();
		return executor;
	}
	
	@Bean
	public SendGrid sendGrid() {
		return new SendGrid(apiKey);
	}
}
