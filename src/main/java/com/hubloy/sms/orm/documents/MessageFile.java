package com.hubloy.sms.orm.documents;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.hubloy.sms.base.orm.AbstractEntity;
import com.hubloy.sms.constants.FileStatus;
import com.hubloy.sms.orm.account.Account;
import com.hubloy.sms.orm.account.User;

/**
 * For messages to send
 * 
 * @author Paul
 *
 */
@Entity
public class MessageFile  extends AbstractEntity{

	private static final long serialVersionUID = -3811061713941255682L;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "account")
	private Account account;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user")
	private User user;
	
	@Column(name = "path")
	private String path;

	@Column(name = "processed", nullable = false, columnDefinition = "boolean default false")
	private boolean processed = false;
	
	@Enumerated(EnumType.STRING)
	@Column(name ="file_status")
	private FileStatus fileStatus;

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public boolean isProcessed() {
		return processed;
	}

	public void setProcessed(boolean processed) {
		this.processed = processed;
	}

	public FileStatus getFileStatus() {
		return fileStatus;
	}

	public void setFileStatus(FileStatus fileStatus) {
		this.fileStatus = fileStatus;
	}
}
