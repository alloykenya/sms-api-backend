package com.hubloy.sms.orm.settings;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.hubloy.sms.base.orm.AbstractEntity;

/**
 * System Settings
 * This holds conversion rates and other things
 * 
 * @author Paul
 *
 */
@Entity
public class SystemSettings extends AbstractEntity{

	private static final long serialVersionUID = 6487389312582013116L;
	
	@Column(name = "setting", nullable = false, unique = true)
	private String setting;
	
	@Column(name = "details", nullable = true, length=1000000)
	private String details;

	public String getSetting() {
		return setting;
	}

	public void setSetting(String setting) {
		this.setting = setting;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}
}