package com.hubloy.sms.orm.settings;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.hubloy.sms.base.orm.AbstractEntity;

@Entity
public class SMSAPISettings extends AbstractEntity{

	private static final long serialVersionUID = -1520477521883691670L;
	
	@Column(name = "username", nullable = false)
	private String username;

	@Column(name = "api_key", nullable = false)
	private String apiKey;
	
	@Column(name = "credits", nullable = true)
	private Long credits;
	
	@Column(name = "amount", nullable = true)
	private String amount;
	
	@Column(name = "rate", nullable = true)
	private Long rate;
	
	@Column(name = "active", nullable = false, columnDefinition = "boolean default false")
	private boolean active = false;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public Long getCredits() {
		return credits;
	}

	public void setCredits(Long credits) {
		this.credits = credits;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public Long getRate() {
		return rate;
	}

	public void setRate(Long rate) {
		this.rate = rate;
	}
}