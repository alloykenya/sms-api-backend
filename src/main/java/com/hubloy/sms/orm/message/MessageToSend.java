package com.hubloy.sms.orm.message;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.hubloy.sms.base.orm.AbstractEntity;
import com.hubloy.sms.constants.MessageStatus;
import com.hubloy.sms.orm.account.Account;
import com.hubloy.sms.orm.contacts.ContactGroup;
import com.hubloy.sms.orm.senders.SMSSender;

@Entity
public class MessageToSend extends AbstractEntity{

	private static final long serialVersionUID = 8189642759794173818L;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "account")
	private Account account;
	
	@Column(name = "msisdn", nullable = true)
	private String msisdn;
	
	@Column(name = "message", nullable = true, length=1000000)
	private String message;
	
	@Column(name = "schedule", nullable = false)
	private Date schedule;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "sender")
	private SMSSender smsSender;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "contact_group")
	private ContactGroup contactGroup;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "template")
	private MessageTemplate messageTemplate;
	
	@Enumerated(EnumType.STRING)
	@Column(name ="message_status")
	private MessageStatus messageStatus;
	
	@Column(name = "response", nullable = true)
	private String response;

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getSchedule() {
		return schedule;
	}

	public void setSchedule(Date schedule) {
		this.schedule = schedule;
	}

	public SMSSender getSmsSender() {
		return smsSender;
	}

	public void setSmsSender(SMSSender smsSender) {
		this.smsSender = smsSender;
	}

	public ContactGroup getContactGroup() {
		return contactGroup;
	}

	public void setContactGroup(ContactGroup contactGroup) {
		this.contactGroup = contactGroup;
	}

	public MessageTemplate getMessageTemplate() {
		return messageTemplate;
	}

	public void setMessageTemplate(MessageTemplate messageTemplate) {
		this.messageTemplate = messageTemplate;
	}

	public MessageStatus getMessageStatus() {
		return messageStatus;
	}

	public void setMessageStatus(MessageStatus messageStatus) {
		this.messageStatus = messageStatus;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}
}
