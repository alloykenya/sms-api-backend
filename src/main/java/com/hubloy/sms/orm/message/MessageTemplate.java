package com.hubloy.sms.orm.message;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.hubloy.sms.base.orm.AbstractEntity;
import com.hubloy.sms.orm.account.Account;

@Entity
public class MessageTemplate extends AbstractEntity{

	private static final long serialVersionUID = -8600470005173845256L;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "account")
	private Account account;
	
	@Column(name = "name", nullable = false)
	private String name;
	
	@Column(name = "template", nullable = false, length=1000000)
	private String template;

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}
}
