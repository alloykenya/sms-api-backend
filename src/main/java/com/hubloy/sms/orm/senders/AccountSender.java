package com.hubloy.sms.orm.senders;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.hubloy.sms.base.orm.AbstractEntity;
import com.hubloy.sms.orm.account.Account;

@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "sender", "account" }))
@Entity
public class AccountSender extends AbstractEntity{

	private static final long serialVersionUID = -2872394172924551568L;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "account")
	private Account account;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "sender")
	private SMSSender smsSender;

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public SMSSender getSmsSender() {
		return smsSender;
	}

	public void setSmsSender(SMSSender smsSender) {
		this.smsSender = smsSender;
	}
}
