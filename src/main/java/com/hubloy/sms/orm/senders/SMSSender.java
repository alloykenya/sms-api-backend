package com.hubloy.sms.orm.senders;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.hubloy.sms.base.orm.AbstractEntity;
import com.hubloy.sms.orm.settings.SMSAPISettings;

@Entity
public class SMSSender extends AbstractEntity{
	
	private static final long serialVersionUID = 2542845645784795505L;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "api_settingss")
	private SMSAPISettings settings;
	
	@Column(name = "sender", nullable = false, unique = true)
	private String sender;

	public SMSAPISettings getSettings() {
		return settings;
	}

	public void setSettings(SMSAPISettings settings) {
		this.settings = settings;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}
}
