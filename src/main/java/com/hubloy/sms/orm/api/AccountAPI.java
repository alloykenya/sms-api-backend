package com.hubloy.sms.orm.api;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.hubloy.sms.base.orm.AbstractEntity;
import com.hubloy.sms.orm.account.Account;

@Entity
public class AccountAPI extends AbstractEntity{

	private static final long serialVersionUID = 5482415454749208318L;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "account")
	private Account account;
	
	@Column(name = "api_key", nullable = false, unique = true)
	private String apiKey;

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
}
