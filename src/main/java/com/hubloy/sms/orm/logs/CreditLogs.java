package com.hubloy.sms.orm.logs;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.hubloy.sms.base.orm.AbstractEntity;
import com.hubloy.sms.orm.account.Account;
import com.hubloy.sms.orm.account.User;

@Entity
public class CreditLogs extends AbstractEntity{

	private static final long serialVersionUID = 5936402852532613417L;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "account")
	private Account account;
	
	@Column(name = "credits", nullable = true)
	private Long credits;
	
	@Column(name = "message_credits", nullable = true)
	private Long messageCredits;
	
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user")
	private User user;


	public Account getAccount() {
		return account;
	}


	public void setAccount(Account account) {
		this.account = account;
	}


	public Long getCredits() {
		return credits;
	}


	public void setCredits(Long credits) {
		this.credits = credits;
	}


	public Long getMessageCredits() {
		return messageCredits;
	}


	public void setMessageCredits(Long messageCredits) {
		this.messageCredits = messageCredits;
	}


	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}
}
