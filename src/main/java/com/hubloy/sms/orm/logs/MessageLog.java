package com.hubloy.sms.orm.logs;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.hubloy.sms.base.orm.AbstractEntity;
import com.hubloy.sms.orm.account.Account;

@Entity
public class MessageLog extends AbstractEntity{

	private static final long serialVersionUID = 2486125369057543313L;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "account")
	private Account account;
	
	@Column(name = "msisdn", nullable = true)
	private String msisdn;
	
	@Column(name = "message", nullable = true, length=1000000)
	private String message;

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}