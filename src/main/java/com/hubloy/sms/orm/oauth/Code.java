/**
 *
 */
package com.hubloy.sms.orm.oauth;

import java.io.Serializable;
import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


/**
 * Add a brief description of OathCode
 * 
 * @author <a href="mailto:enter email address">Paul Kevin</a>
 * @verion enter version, Sep 17, 2015
 * @since  enter jdk version
 */
@Entity(name="oauth_code")
public class Code implements Serializable{

	private static final long serialVersionUID = 4880695249047358572L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true, updatable=false)
	private Long id;
	
	@Column(name = "code")
	private String code;
	
	@Column(name = "authentication")
	private Blob authentication;

	
	
	public Long getId() {
	
		return id;
	}


	
	public void setId(Long id) {
	
		this.id = id;
	}


	public String getCode() {
	
		return code;
	}

	
	public void setCode(String code) {
	
		this.code = code;
	}

	
	public Blob getAuthentication() {
	
		return authentication;
	}

	
	public void setAuthentication(Blob authentication) {
	
		this.authentication = authentication;
	}
}
