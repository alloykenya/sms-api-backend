/**
 *
 */
package com.hubloy.sms.orm.oauth;

import java.io.Serializable;
import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


/**
 * Add a brief description of ClientToken
 * 
 * @author <a href="mailto:enter email address">Paul Kevin</a>
 * @verion enter version, Sep 17, 2015
 * @since  enter jdk version
 */
@Entity(name="oauth_client_token")
public class ClientToken implements Serializable{

	private static final long serialVersionUID = -1773425360140908641L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true, updatable=false)
	private Long id;
	
	@Column(name = "token_id")
	private String tokenId;
	
	@Column(name = "token")
	private Blob token;
	
	@Column(name = "authentication_id")
	private String authenticationId;
	
	@Column(name = "user_name")
	private String userName;
	
	@Column(name = "client_id")
	private String clientId;

	
	
	public Long getId() {
	
		return id;
	}


	
	public void setId(Long id) {
	
		this.id = id;
	}


	public String getTokenId() {
	
		return tokenId;
	}

	
	public void setTokenId(String tokenId) {
	
		this.tokenId = tokenId;
	}

	
	public Blob getToken() {
	
		return token;
	}

	
	public void setToken(Blob token) {
	
		this.token = token;
	}

	
	public String getAuthenticationId() {
	
		return authenticationId;
	}

	
	public void setAuthenticationId(String authenticationId) {
	
		this.authenticationId = authenticationId;
	}

	
	public String getUserName() {
	
		return userName;
	}

	
	public void setUserName(String userName) {
	
		this.userName = userName;
	}

	
	public String getClientId() {
	
		return clientId;
	}

	
	public void setClientId(String clientId) {
	
		this.clientId = clientId;
	}
}