/**
 *
 */
package com.hubloy.sms.orm.oauth;

import java.io.Serializable;
import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


/**
 * Add a brief description of RefreshToken
 * 
 * @author <a href="mailto:enter email address">Paul Kevin</a>
 * @verion enter version, Sep 17, 2015
 * @since  enter jdk version
 */
@Entity(name="oauth_refresh_token")
public class RefreshToken implements Serializable{
	
	private static final long serialVersionUID = -366828630621424272L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true, updatable=false)
	private Long id;
	
	@Column(name = "token_id")
	private String tokenId;
	
	@Column(name = "token")
	private Blob token;
	
	@Column(name = "authentication")
	private Blob authentication;

	
	
	public Long getId() {
	
		return id;
	}


	
	public void setId(Long id) {
	
		this.id = id;
	}


	public String getTokenId() {
	
		return tokenId;
	}

	
	public void setTokenId(String tokenId) {
	
		this.tokenId = tokenId;
	}

	
	public Blob getToken() {
	
		return token;
	}

	
	public void setToken(Blob token) {
	
		this.token = token;
	}

	
	public Blob getAuthentication() {
	
		return authentication;
	}

	
	public void setAuthentication(Blob authentication) {
	
		this.authentication = authentication;
	}
}
