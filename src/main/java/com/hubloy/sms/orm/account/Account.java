package com.hubloy.sms.orm.account;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.hubloy.sms.base.orm.AbstractEntity;
import com.hubloy.sms.constants.AccountStatus;
import com.hubloy.sms.constants.AccountType;

@Entity
public class Account extends AbstractEntity{

	private static final long serialVersionUID = -5334067616027575840L;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "admin", unique = true)
	private User admin;
	
	@Column(name = "account_name", nullable = false, unique = true)
	private String accountName;
	
	@Enumerated(EnumType.STRING)
	@Column(name ="account_status")
	private AccountStatus accountStatus;
	
	@Enumerated(EnumType.STRING)
	@Column(name ="account_type")
	private AccountType accountType;
	
	@Column(name = "credits", nullable = true)
	private Long credits;

	@Column(name = "message_credits", nullable = true)
	private Long messageCredits;
	
	@Column(name = "lifetime_credits", nullable = true)
	private Long lifeTimeCredits;
	
	@Column(name = "message_counter", nullable = true)
	private Long messageCounter;

	public User getAdmin() {
		return admin;
	}

	public void setAdmin(User admin) {
		this.admin = admin;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public AccountStatus getAccountStatus() {
		return accountStatus;
	}

	public void setAccountStatus(AccountStatus accountStatus) {
		this.accountStatus = accountStatus;
	}

	public AccountType getAccountType() {
		return accountType;
	}

	public void setAccountType(AccountType accountType) {
		this.accountType = accountType;
	}

	public Long getCredits() {
		return credits;
	}

	public void setCredits(Long credits) {
		this.credits = credits;
	}
	
	public Long getMessageCredits() {
		return messageCredits;
	}

	public void setMessageCredits(Long messageCredits) {
		this.messageCredits = messageCredits;
	}

	public Long getLifeTimeCredits() {
		return lifeTimeCredits;
	}

	public void setLifeTimeCredits(Long lifeTimeCredits) {
		this.lifeTimeCredits = lifeTimeCredits;
	}

	public Long getMessageCounter() {
		return messageCounter;
	}

	public void setMessageCounter(Long messageCounter) {
		this.messageCounter = messageCounter;
	}
}
