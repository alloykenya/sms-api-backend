package com.hubloy.sms.orm.account;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.hubloy.sms.base.orm.AbstractEntity;

@Table(name = "user")
@Entity
public class User extends AbstractEntity {

	private static final long serialVersionUID = 4447888595756617568L;

	@Column(name = "login", unique = true, nullable = false)
	private String login;

	@Column(name = "password", nullable = false)
	private String password;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "user_role", joinColumns = { @JoinColumn(name = "user_id") }, inverseJoinColumns = {
			@JoinColumn(name = "role_id") })
	private Set<Role> roles = new HashSet<Role>();

	@Column(name = "active", nullable = false, columnDefinition = "boolean default true")
	private boolean active = true;

	@OneToOne(mappedBy = "user", fetch = FetchType.EAGER)
	private Profile profile;

	public User() {
	}

	public User(User user) {
		super();
		this.id = user.getId();
		this.itemId = user.getItemId();
		this.login = user.getLogin();
		this.password = user.getPassword();
		this.roles = user.getRoles();
		this.active = user.isActive();
		this.profile = user.getProfile();
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}
}
