package com.hubloy.sms.orm.account;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.hubloy.sms.base.orm.AbstractEntity;
import com.hubloy.sms.constants.AccountRoles;

/**
 * 
 * Account User
 * Users who are not super admins globally
 * 
 * @author Paul Kevin
 *
 * @version enter version, 4 Apr 2018
 *
 * @since  jdk 1.8
 */
@Entity
@Table(name="account_user", uniqueConstraints = {@UniqueConstraint (columnNames = {"user","account"})})
public class AccountUser extends AbstractEntity{

	private static final long serialVersionUID = -5598295838222683201L;

	@Enumerated(EnumType.STRING)
	@Column(name ="role")
	private AccountRoles accountRoles;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user", unique = true)
	private User user;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "account")
	private Account account;

	public AccountRoles getAccountRoles() {
		return accountRoles;
	}

	public void setAccountRoles(AccountRoles accountRoles) {
		this.accountRoles = accountRoles;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}
}