package com.hubloy.sms.orm.account;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;

import org.springframework.security.core.GrantedAuthority;

import com.hubloy.sms.base.orm.AbstractEntity;

/**
 * 
 * Role
 * Only roles we have : Super Admin, Customer
 * 
 * @author Paul Kevin
 *
 * @version enter version, 4 Apr 2018
 *
 * @since  jdk 1.8
 */
@Entity
public class Role extends AbstractEntity implements GrantedAuthority{
	
	private static final long serialVersionUID = -3018082554972766449L;

	@Column(name = "name", nullable = false)
	private String name;
	
	@Column(name = "simple_name", nullable = false)
	private String simpleName;
	
	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "roles")
	private Set<User> users = new HashSet<User>();
	
	@Column(name = "general", nullable = false, columnDefinition = "boolean default true")
	private boolean general = true;

	@Override
	public String getAuthority() {
		return name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSimpleName() {
		return simpleName;
	}

	public void setSimpleName(String simpleName) {
		this.simpleName = simpleName;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	public boolean isGeneral() {
		return general;
	}

	public void setGeneral(boolean general) {
		this.general = general;
	}
}
