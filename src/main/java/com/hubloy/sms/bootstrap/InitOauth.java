package com.hubloy.sms.bootstrap;

import javax.transaction.Transactional;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.hubloy.sms.base.BaseHelper;
import com.hubloy.sms.orm.oauth.ClientDetails;
import com.hubloy.sms.repository.oauth.ClientDetailsRepository;

@Service
public class InitOauth extends BaseHelper implements InitializingBean{
	
	@Autowired
	private ClientDetailsRepository clientDetailsRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	@Transactional
	public void afterPropertiesSet() throws Exception {
		log().debug("Checking if oauth is configured");
		if (clientDetailsRepository.count() <= 0) {
			log().info("Setting up oauth details");
			ClientDetails clientDetails = new ClientDetails();
			clientDetails.setClientId("smsapi");
			clientDetails.setClientSecret(passwordEncoder.encode("juQy1KpVeV29nJ9UeWLpc89iANDxXAYAvoEa4y6c"));
			clientDetails.setScope("read,write,trust");
			clientDetails
					.setAuthorizedGrantTypes("password,authorization_code,refresh_token,implicit");
			clientDetails.setAuthorities("USER");
			clientDetails.setAccessTokenValidity(31536000);

			clientDetailsRepository.saveAndFlush(clientDetails);

			clientDetailsRepository.saveAndFlush(clientDetails);
			log().info("Done setting up oauth details");
		}
		log().debug("Done checking if oauth is configured");
	}

}