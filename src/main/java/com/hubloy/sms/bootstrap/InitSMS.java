package com.hubloy.sms.bootstrap;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hubloy.sms.base.BaseHelper;
import com.hubloy.sms.service.message.SMSService;

@Service
public class InitSMS extends BaseHelper implements InitializingBean{
	
	@Autowired
	private SMSService smsService;

	@Override
	public void afterPropertiesSet() throws Exception {
		smsService.process();
	}

}
