package com.hubloy.sms.bootstrap;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.hubloy.sms.base.BaseHelper;
import com.hubloy.sms.helpers.DirectoryHelpers;
import com.hubloy.sms.helpers.FileUtils;

@Service
public class InitDirectories extends BaseHelper implements InitializingBean{
	
	@Value("${app.ctx}")
	private String path;

	@Override
	public void afterPropertiesSet() throws Exception {
		DirectoryHelpers.setDirectories(path);
		FileUtils.createdir(DirectoryHelpers.getRootDirectory());
		FileUtils.createdir(DirectoryHelpers.getSystemDirectory());
		FileUtils.createdir(DirectoryHelpers.getContactDirectory());
		FileUtils.createdir(DirectoryHelpers.getMessageDirectory());
	}

}
