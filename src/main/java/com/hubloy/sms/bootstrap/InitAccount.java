package com.hubloy.sms.bootstrap;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.hubloy.sms.base.BaseHelper;
import com.hubloy.sms.constants.AccountRoles;
import com.hubloy.sms.constants.AccountStatus;
import com.hubloy.sms.constants.AccountType;
import com.hubloy.sms.orm.account.Account;
import com.hubloy.sms.orm.account.AccountUser;
import com.hubloy.sms.orm.account.Profile;
import com.hubloy.sms.orm.account.Role;
import com.hubloy.sms.orm.account.User;
import com.hubloy.sms.repository.account.AccountRepository;
import com.hubloy.sms.repository.account.AccountUserRepository;
import com.hubloy.sms.repository.account.ProfileRepository;
import com.hubloy.sms.repository.account.RoleRepository;
import com.hubloy.sms.repository.account.UserRepository;

@Service
public class InitAccount extends BaseHelper implements InitializingBean{
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private ProfileRepository profileRepository;
	
	@Autowired
	private AccountRepository accountRepository;
	
	@Autowired
	private AccountUserRepository accountUserRepository;

	@Override
	public void afterPropertiesSet() throws Exception {
		log().debug("Initializing account details");
		if(roleRepository.count() <= 0) {
			log().info("Setting up roles");
			Role role = new Role();
			role.setName("SUPERADMIN");
			role.setSimpleName("Super Admin");
			role.setGeneral(false);
			roleRepository.saveAndFlush(role);
			
			role = new Role();
			role.setName("ADMIN");
			role.setSimpleName("Admin");
			role.setGeneral(true);
			roleRepository.saveAndFlush(role);
			
			role = new Role();
			role.setName("CUSTOMER");
			role.setSimpleName("Customer");
			role.setGeneral(true);
			roleRepository.saveAndFlush(role);
		}
		
		if(userRepository.count() <= 0) {
			log().info("Setting up default user");
			Set<Role> roles = new HashSet<Role>();
			roles.add(roleRepository.findByName("SUPERADMIN"));
			User user = new User();
			user.setLogin("owen");
			user.setPassword(passwordEncoder.encode("owen@123"));
			user.setActive(true);
			user.setRoles(roles);
			User u = userRepository.saveAndFlush(user);
			Profile profile = new Profile();
			profile.setUser(u);
			profile.setEmail("ceomobtechsolutions@gmail.com");
			profile.setFullnames("Owen Banda");
			profileRepository.saveAndFlush(profile);
			
			Account account = new Account();
			account.setAccountName("Mobtech Solutions");
			account.setAccountType(AccountType.System);
			account.setAdmin(u);
			account.setAccountStatus(AccountStatus.Active);
			Account a = accountRepository.saveAndFlush(account);
			
			AccountUser accountUser = new AccountUser();
			accountUser.setAccountRoles(AccountRoles.Super_Admin);
			accountUser.setAccount(a);
			accountUser.setUser(u);
			accountUserRepository.saveAndFlush(accountUser);
			
			log().info("Done setting up default user");
			
		}
		log().debug("Done initialising account details");
	}
}
