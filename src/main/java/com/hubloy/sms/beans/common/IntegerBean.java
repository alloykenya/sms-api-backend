package com.hubloy.sms.beans.common;

import com.hubloy.sms.base.GenericMessage;

public class IntegerBean extends GenericMessage{

	private static final long serialVersionUID = 7280222217962495892L;
	
	private long number;

	public long getNumber() {
		return number;
	}

	public void setNumber(long number) {
		this.number = number;
	}
}