package com.hubloy.sms.beans.common;

import com.hubloy.sms.base.GenericMessage;

public class DataBean extends GenericMessage{

	private static final long serialVersionUID = -2353482720098526482L;

	private Long messagesSent;
	
	private Long messagesPending;
	
	private Long contacts;
	
	private Long groups;

	public Long getMessagesSent() {
		return messagesSent;
	}

	public void setMessagesSent(Long messagesSent) {
		this.messagesSent = messagesSent;
	}

	public Long getMessagesPending() {
		return messagesPending;
	}

	public void setMessagesPending(Long messagesPending) {
		this.messagesPending = messagesPending;
	}

	public Long getContacts() {
		return contacts;
	}

	public void setContacts(Long contacts) {
		this.contacts = contacts;
	}

	public Long getGroups() {
		return groups;
	}

	public void setGroups(Long groups) {
		this.groups = groups;
	}
}