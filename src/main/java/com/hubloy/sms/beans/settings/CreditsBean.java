package com.hubloy.sms.beans.settings;

import com.hubloy.sms.base.GenericMessage;

public class CreditsBean extends GenericMessage{

	private static final long serialVersionUID = 6860348197531095275L;

	private Long total;
	
	private Long issued;
	
	private Long available;
	
	private String credits;

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public Long getIssued() {
		return issued;
	}

	public void setIssued(Long issued) {
		this.issued = issued;
	}

	public Long getAvailable() {
		return available;
	}

	public void setAvailable(Long available) {
		this.available = available;
	}

	public String getCredits() {
		return credits;
	}

	public void setCredits(String credits) {
		this.credits = credits;
	}
}
