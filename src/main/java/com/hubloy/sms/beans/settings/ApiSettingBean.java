package com.hubloy.sms.beans.settings;

import com.hubloy.sms.base.GenericMessage;

public class ApiSettingBean extends GenericMessage{

	private static final long serialVersionUID = 1019220683682834240L;

	private String id;
	
	private String username;
	
	private String apiKey;
	
	private Long credits;
	
	private Long rate;
	
	private boolean active;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public Long getCredits() {
		return credits;
	}

	public void setCredits(Long credits) {
		this.credits = credits;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Long getRate() {
		return rate;
	}

	public void setRate(Long rate) {
		this.rate = rate;
	}
}
