package com.hubloy.sms.beans.message;

import com.hubloy.sms.base.GenericMessage;

public class TemplateBean extends GenericMessage{

	private static final long serialVersionUID = -6185859060100665725L;

	private String id;
	
	private String name;
	
	private String template;
	
	private String createdAt;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
}
