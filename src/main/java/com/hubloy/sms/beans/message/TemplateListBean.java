package com.hubloy.sms.beans.message;

import java.util.List;

public class TemplateListBean {

	private long total;

	private List<TemplateBean> rows;

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public List<TemplateBean> getRows() {
		return rows;
	}

	public void setRows(List<TemplateBean> rows) {
		this.rows = rows;
	}
}