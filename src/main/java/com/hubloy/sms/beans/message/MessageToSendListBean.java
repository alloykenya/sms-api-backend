package com.hubloy.sms.beans.message;

import java.util.List;

public class MessageToSendListBean {

	private long total;

	private List<MessageToSendBean> rows;

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public List<MessageToSendBean> getRows() {
		return rows;
	}

	public void setRows(List<MessageToSendBean> rows) {
		this.rows = rows;
	}
}