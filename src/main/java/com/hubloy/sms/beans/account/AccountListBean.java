package com.hubloy.sms.beans.account;

import java.util.List;

public class AccountListBean {
	
	private long total;
	
	private List<AccountBean> rows;

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public List<AccountBean> getRows() {
		return rows;
	}

	public void setRows(List<AccountBean> rows) {
		this.rows = rows;
	}
}
