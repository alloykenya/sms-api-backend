package com.hubloy.sms.beans.account;

import java.util.List;

public class AccountUserListBean {

	private long total;

	private List<AccountUserBean> rows;

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public List<AccountUserBean> getRows() {
		return rows;
	}

	public void setRows(List<AccountUserBean> rows) {
		this.rows = rows;
	}
}
