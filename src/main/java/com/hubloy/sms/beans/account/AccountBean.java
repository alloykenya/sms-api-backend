package com.hubloy.sms.beans.account;

import com.hubloy.sms.base.GenericMessage;

public class AccountBean extends GenericMessage {

	private static final long serialVersionUID = 563171393669748677L;
	
	private String id;
	
	private String name;
	
	private String accountStatus;
	
	private Long credits;
	
	private Long messageCredits;
	
	private Long users;
	
	private String admin;
	
	private String adminId;
	
	private boolean system;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAccountStatus() {
		return accountStatus;
	}

	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}

	public Long getCredits() {
		return credits;
	}

	public void setCredits(Long credits) {
		this.credits = credits;
	}

	public Long getMessageCredits() {
		return messageCredits;
	}

	public void setMessageCredits(Long messageCredits) {
		this.messageCredits = messageCredits;
	}

	public Long getUsers() {
		return users;
	}

	public void setUsers(Long users) {
		this.users = users;
	}

	public String getAdmin() {
		return admin;
	}

	public void setAdmin(String admin) {
		this.admin = admin;
	}

	public String getAdminId() {
		return adminId;
	}

	public void setAdminId(String adminId) {
		this.adminId = adminId;
	}

	public boolean isSystem() {
		return system;
	}

	public void setSystem(boolean system) {
		this.system = system;
	}
}
