package com.hubloy.sms.beans.account;

import com.hubloy.sms.base.GenericMessage;
import com.hubloy.sms.constants.ResponseStatus;

public class AccountUserBean extends GenericMessage{
	
	private static final long serialVersionUID = -1463097814007030277L;

	private String id;
	
	private String name;
	
	private String email;
	
	private String login;
	
	private String accountStatus;
	
	private Long credits;
	
	private Long messageCredits;
	
	private boolean isAdmin;
	
	private String type;
	
	private boolean active;
	
	private String acccountId;
	
	private String createdAt;
	
	private String updatedAt;

	public AccountUserBean() {
		super();
	}

	public AccountUserBean(ResponseStatus responseStatus, String message) {
		super(responseStatus, message);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAccountStatus() {
		return accountStatus;
	}

	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}

	public Long getCredits() {
		return credits;
	}

	public void setCredits(Long credits) {
		this.credits = credits;
	}

	public Long getMessageCredits() {
		return messageCredits;
	}

	public void setMessageCredits(Long messageCredits) {
		this.messageCredits = messageCredits;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getAcccountId() {
		return acccountId;
	}

	public void setAcccountId(String acccountId) {
		this.acccountId = acccountId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
}
