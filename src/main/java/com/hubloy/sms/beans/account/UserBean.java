package com.hubloy.sms.beans.account;

import com.hubloy.sms.base.GenericMessage;

public class UserBean extends GenericMessage{

	private static final long serialVersionUID = -5110050681830634714L;

	private String id;
	
	private String login;
	
	private String fullnames;
	
	private String email;
	
	private String phone;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getFullnames() {
		return fullnames;
	}

	public void setFullnames(String fullnames) {
		this.fullnames = fullnames;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
}
