package com.hubloy.sms.beans.senders;

import com.hubloy.sms.base.GenericMessage;

public class SMSSenderBean extends GenericMessage{

	private static final long serialVersionUID = -8780867011582823713L;

	private String id;
	
	private String sender;
	
	private String settingId;
	
	private Long accounts;
	
	private String accountSenderid;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getSettingId() {
		return settingId;
	}

	public void setSettingId(String settingId) {
		this.settingId = settingId;
	}

	public Long getAccounts() {
		return accounts;
	}

	public void setAccounts(Long accounts) {
		this.accounts = accounts;
	}

	public String getAccountSenderid() {
		return accountSenderid;
	}

	public void setAccountSenderid(String accountSenderid) {
		this.accountSenderid = accountSenderid;
	}
}
