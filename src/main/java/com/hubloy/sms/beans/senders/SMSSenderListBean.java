package com.hubloy.sms.beans.senders;

import java.util.List;

public class SMSSenderListBean {

	private long total;

	private List<SMSSenderBean> rows;

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public List<SMSSenderBean> getRows() {
		return rows;
	}

	public void setRows(List<SMSSenderBean> rows) {
		this.rows = rows;
	}
}