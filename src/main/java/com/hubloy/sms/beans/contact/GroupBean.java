package com.hubloy.sms.beans.contact;

import com.hubloy.sms.base.GenericMessage;

public class GroupBean extends GenericMessage{

	private static final long serialVersionUID = 3838931621144213303L;

	private String id;
	
	private String name;
	
	private Long contacts;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getContacts() {
		return contacts;
	}

	public void setContacts(Long contacts) {
		this.contacts = contacts;
	}
}