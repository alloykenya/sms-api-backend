package com.hubloy.sms.beans.contact;

import com.hubloy.sms.base.GenericMessage;

public class ContactBean extends GenericMessage{

	private static final long serialVersionUID = -8962477538269760774L;

	private String id;
	
	private String name;
	
	private String phone;
	
	private String group;
	
	private String createdAt;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
}