package com.hubloy.sms.beans.contact;

import java.util.List;

public class ContactListBean {
	
	private long total;

	private List<ContactBean> rows;

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public List<ContactBean> getRows() {
		return rows;
	}

	public void setRows(List<ContactBean> rows) {
		this.rows = rows;
	}
}
