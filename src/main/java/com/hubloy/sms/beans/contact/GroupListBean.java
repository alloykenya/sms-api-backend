package com.hubloy.sms.beans.contact;

import java.util.List;

public class GroupListBean {
	
	private long total;

	private List<GroupBean> rows;

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public List<GroupBean> getRows() {
		return rows;
	}

	public void setRows(List<GroupBean> rows) {
		this.rows = rows;
	}
}
