package com.hubloy.sms.view;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import com.hubloy.sms.beans.message.ReportMessageBean;

public class ExcelView extends AbstractXlsView {

	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		if (model.containsKey("account")) {
			// change the file name
			String account = model.get("account").toString();
			Long name = Calendar.getInstance().getTimeInMillis();
			response.setHeader("Content-Disposition", "attachment; filename=\"" + account + " - " + name + ".xls\"");
			@SuppressWarnings("unchecked")
			List<ReportMessageBean> list = (List<ReportMessageBean>) model.get("messages");

			Sheet sheet = workbook.createSheet("Message Report");
			sheet.setDefaultColumnWidth(30);

			CellStyle style = workbook.createCellStyle();
			Font font = workbook.createFont();
			font.setFontName("Arial");
			style.setFillForegroundColor(IndexedColors.BLACK.index);
			style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			font.setBold(true);
			font.setColor(IndexedColors.WHITE.index);
			style.setFont(font);

			Row header = sheet.createRow(0);
			header.createCell(0).setCellValue("Status");
			header.getCell(0).setCellStyle(style);
			header.createCell(1).setCellValue("Phone");
			header.getCell(1).setCellStyle(style);
			header.createCell(2).setCellValue("Message");
			header.getCell(2).setCellStyle(style);
			header.createCell(3).setCellValue("Date Created");
			header.getCell(3).setCellStyle(style);
			header.createCell(4).setCellValue("Date Sent");
			header.getCell(4).setCellStyle(style);
			header.createCell(5).setCellValue("Schedule");
			header.getCell(5).setCellStyle(style);
			header.createCell(6).setCellValue("Response");
			header.getCell(6).setCellStyle(style);
			header.createCell(7).setCellValue("Sender");
			header.getCell(7).setCellStyle(style);

			int rowCount = 1;

			for (ReportMessageBean bean : list) {
				Row userRow = sheet.createRow(rowCount++);
				userRow.createCell(0).setCellValue(bean.getStatus());
				userRow.createCell(1).setCellValue(bean.getNumber());
				userRow.createCell(2).setCellValue(bean.getMessage());
				userRow.createCell(3).setCellValue(bean.getCreatedAt());
				userRow.createCell(4).setCellValue(bean.getUpdatedAt());
				userRow.createCell(5).setCellValue(bean.getSchedule());
				userRow.createCell(6).setCellValue(bean.getResponse());
				userRow.createCell(7).setCellValue(bean.getSender());
			}
		}

	}

}
