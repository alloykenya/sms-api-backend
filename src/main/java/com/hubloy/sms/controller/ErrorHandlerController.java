/**
 *
 */
package com.hubloy.sms.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hubloy.sms.base.GenericMessage;
import com.hubloy.sms.constants.ResponseStatus;

/**
 * Add a brief description of ErrorController
 * 
 * @author <a href="mailto:enter email address">Paul Kevin</a>
 * @verion enter version, May 19, 2016
 * @since enter jdk version
 */
@RestController
public class ErrorHandlerController implements ErrorController {

	private static final String PATH = "/error";

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.boot.autoconfigure.web.ErrorController#getErrorPath()
	 */
	@Override
	public String getErrorPath() {

		return PATH;
	}

	/**
	 * 
	 * Error handling
	 * <p>
	 * 
	 * @return
	 */
	@RequestMapping(value = PATH)
	public GenericMessage error(HttpServletRequest request) {

		return new GenericMessage(ResponseStatus.ERROR,"An error occured completing your request");
	}

}
