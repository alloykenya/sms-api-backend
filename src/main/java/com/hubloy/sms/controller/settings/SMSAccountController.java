package com.hubloy.sms.controller.settings;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hubloy.sms.base.GenericMessage;
import com.hubloy.sms.beans.settings.ApiSettingBean;
import com.hubloy.sms.beans.settings.CreditsBean;
import com.hubloy.sms.orm.account.User;
import com.hubloy.sms.service.settings.SMSAccountService;

@RestController
public class SMSAccountController {

	@Autowired
	private SMSAccountService smsAccountService;
	
	/**
	 * Get Settings
	 * 
	 * @return
	 */
	@GetMapping(path = "/api/settings")
	public ApiSettingBean getSettings() {
		return smsAccountService.getSettings();
	}
	
	/**
	 * Save From API
	 * 
	 * @param user
	 * @param username
	 * @param apiKey
	 * @return
	 */
	@PostMapping(path = "/api/settings/save")
	public GenericMessage saveSettings(@AuthenticationPrincipal User user, 
			@RequestParam(value = "username", required = true) String username, 
			@RequestParam(value = "api_key", required = true) String apiKey,
			@RequestParam(value = "rate", required = true) Long rate) {
		return smsAccountService.saveSettings(user, username, apiKey, rate);
	}
	
	/**
	 * Refresh from API
	 * 
	 * @param user
	 * @return
	 */
	@GetMapping(path = "/api/settings/refresh")
	public GenericMessage refreshBalance() {
		return smsAccountService.refreshBalance();
	}
	
	/**
	 * Get Credits
	 * 
	 * @return
	 */
	@GetMapping(path = "/api/settings/credits")
	public CreditsBean getTotalAvailableCredits() {
		return smsAccountService.getTotalAvailableCredits();
	}
}
