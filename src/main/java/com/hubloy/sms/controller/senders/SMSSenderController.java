package com.hubloy.sms.controller.senders;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hubloy.sms.base.GenericMessage;
import com.hubloy.sms.beans.senders.SMSSenderBean;
import com.hubloy.sms.beans.senders.SMSSenderListBean;
import com.hubloy.sms.service.senders.SMSSenderService;

@RestController
public class SMSSenderController {

	@Autowired
	private SMSSenderService smsSenderService;

	/**
	 * List senders
	 * 
	 * @param page
	 * @return
	 */
	@GetMapping(path = "/api/senders/list")
	public SMSSenderListBean listSenders(@RequestParam(value = "page", required = true) int page) {
		return smsSenderService.listSenders(page);
	}

	/**
	 * Pagination account senders
	 * 
	 * @param accountId
	 * @param page
	 * @return
	 */
	@GetMapping(path = "/api/senders/account/list")
	public SMSSenderListBean listAccountSenders(@RequestParam(value = "accountId", required = true) String accountId,
			@RequestParam(value = "page", required = true) int page) {
		return smsSenderService.listAccountSenders(accountId, page);
	}

	/**
	 * List all senders
	 * 
	 * @return
	 */
	@GetMapping(path = "/api/senders/list/all")
	public List<SMSSenderBean> listAllSenders() {
		return smsSenderService.listAllSenders();
	}

	/**
	 * List all senders for account
	 * 
	 * @param accountId
	 * @return
	 */
	@GetMapping(path = "/api/senders/account")
	public List<SMSSenderBean> listAllAccountSenders(
			@RequestParam(value = "account_id", required = true) String accountId) {
		return smsSenderService.listAllAccountSenders(accountId);
	}

	/**
	 * Save Sender
	 * 
	 * @param sender
	 * @return
	 */
	@PostMapping(path = "/api/senders/save")
	public GenericMessage saveSender(@RequestParam(value = "sender", required = true) String sender) {
		return smsSenderService.saveSender(sender);
	}

	/**
	 * 
	 * <p>
	 * Delete sender
	 * </p>
	 * 
	 * @param senderId
	 * @return
	 */
	@PostMapping(path = "/api/senders/delete")
	public GenericMessage removeSender(@RequestParam(value = "senderId", required = true) String senderId) {
		return smsSenderService.removeSender(senderId);
	}

	/**
	 * Add sender to account
	 * 
	 * @param senderId
	 * @param accountId
	 * @return
	 */
	@PostMapping(path = "/api/senders/account/save")
	public GenericMessage addSenderToAccount(@RequestParam(value = "senderId", required = true) String senderId,
			@RequestParam(value = "accountId", required = true) String accountId) {
		return smsSenderService.addSenderToAccount(senderId, accountId);
	}

	/**
	 * Remove sender from account
	 * 
	 * @param id
	 * @return
	 */
	@PostMapping(path = "/api/senders/account/delete")
	public GenericMessage removeSenderFromAccount(@RequestParam(value = "id", required = true) String id) {
		return smsSenderService.removeSenderFromAccount(id);
	}
}
