package com.hubloy.sms.controller.message;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.hubloy.sms.base.GenericMessage;
import com.hubloy.sms.beans.message.MessageToSendListBean;
import com.hubloy.sms.constants.ResponseStatus;
import com.hubloy.sms.orm.account.User;
import com.hubloy.sms.service.message.SMSService;

@RestController
public class SMSController {

	@Autowired
	private SMSService smsService;

	@GetMapping(path = "/api/messages/list")
	public MessageToSendListBean listMessages(@AuthenticationPrincipal User user,
			@RequestParam(value = "page", required = true) int page) {
		return smsService.listMessages(user, page);
	}

	/**
	 * Send Single
	 * 
	 * @param user
	 * @param source
	 * @param contacts
	 * @param group
	 * @param sender
	 * @param schedule
	 * @param message
	 * @return
	 */
	@PostMapping(path = "/api/messages/send/single")
	public GenericMessage sendSingleSMS(@AuthenticationPrincipal User user,
			@RequestParam(value = "source", required = true) String source,
			@RequestParam(value = "contacts", required = false) String contacts,
			@RequestParam(value = "group", required = false) String group,
			@RequestParam(value = "sender", required = true) String sender,
			@RequestParam(value = "schedule", required = false) String schedule,
			@RequestParam(value = "message", required = true) String message) {
		CompletableFuture<GenericMessage> response = smsService.sendSingleSMS(user, source, contacts, group, sender,
				schedule, message);
		CompletableFuture.allOf(response).join();
		try {
			return response.get();
		} catch (InterruptedException e) {
			return new GenericMessage(ResponseStatus.ERROR, "An error occured " + e.getMessage());
		} catch (ExecutionException e) {
			return new GenericMessage(ResponseStatus.ERROR, "An error occured " + e.getMessage());
		}
	}

	/**
	 * Bulk upload
	 * 
	 * @param user
	 * @param file
	 * @param sender
	 * @param schedule
	 * @param template
	 * @return
	 */
	@PostMapping(path = "/api/messages/send/bulk/csv")
	public GenericMessage sendBulkCSV(@AuthenticationPrincipal User user, @RequestParam("file") MultipartFile file,
			@RequestParam(value = "sender", required = true) String sender,
			@RequestParam(value = "schedule", required = false) String schedule,
			@RequestParam(value = "template", required = true) String template) {

		CompletableFuture<GenericMessage> response = smsService.bulkCSVUpload(user, file, sender, schedule, template);
		CompletableFuture.allOf(response).join();
		try {
			return response.get();
		} catch (InterruptedException e) {
			return new GenericMessage(ResponseStatus.ERROR, "An error occured " + e.getMessage());
		} catch (ExecutionException e) {
			return new GenericMessage(ResponseStatus.ERROR, "An error occured " + e.getMessage());
		}
	}

	@PostMapping(path = "/api/messages/send/bulk/xls")
	public GenericMessage bulkExcelUpload(@AuthenticationPrincipal User user, @RequestParam("file") MultipartFile file,
			@RequestParam(value = "sender", required = true) String sender,
			@RequestParam(value = "schedule", required = false) String schedule,
			@RequestParam(value = "template", required = true) String template) {

		CompletableFuture<GenericMessage> response = smsService.bulkExcelUpload(user, file, sender, schedule, template);
		CompletableFuture.allOf(response).join();
		try {
			return response.get();
		} catch (InterruptedException e) {
			return new GenericMessage(ResponseStatus.ERROR, "An error occured " + e.getMessage());
		} catch (ExecutionException e) {
			return new GenericMessage(ResponseStatus.ERROR, "An error occured " + e.getMessage());
		}
	}
	
	/**
	 * Generate report
	 * 
	 * @param user
	 * @param start
	 * @param end
	 * @return
	 */
	@GetMapping(path = "/data/messages/report")
	public ModelAndView generateReport(
			@RequestParam(value = "accountId", required = true) String accountId, 
			@RequestParam(value = "start", required = false) String start, 
			@RequestParam(value = "end", required = false) String end) {
		return smsService.generateReport(accountId, start, end);
	}
}
