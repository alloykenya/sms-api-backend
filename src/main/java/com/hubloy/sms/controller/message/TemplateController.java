package com.hubloy.sms.controller.message;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hubloy.sms.base.GenericMessage;
import com.hubloy.sms.beans.message.TemplateBean;
import com.hubloy.sms.beans.message.TemplateListBean;
import com.hubloy.sms.orm.account.User;
import com.hubloy.sms.service.message.TemplateService;

@RestController
public class TemplateController {
	
	@Autowired
	private TemplateService templateService;
	
	/**
	 * Pagination list templates
	 * 
	 * @param user
	 * @param page
	 * @return
	 */
	@GetMapping(path = "/api/templates/list")
	public TemplateListBean listTemplates(@AuthenticationPrincipal User user, 
			@RequestParam(value = "page", required = true) int page) {
		return templateService.listTemplates(user, page);
	}
	
	/**
	 * List all templates
	 * 
	 * @param user
	 * @return
	 */
	@GetMapping(path = "/api/templates/all")
	public List<TemplateBean> listAllTemplates(@AuthenticationPrincipal User user){
		return templateService.listAllTemplates(user);
	}
	
	/**
	 * Save Template
	 * 
	 * @param user
	 * @param name
	 * @param template
	 * @return
	 */
	@PostMapping(path = "/api/templates/save")
	public GenericMessage saveTemplate(@AuthenticationPrincipal User user, 
			@RequestParam(value = "name", required = true) String name, 
			@RequestParam(value = "template", required = true) String template) {
		return templateService.saveTemplate(user, name, template);
	}
	
	/**
	 * Delete Template
	 * 
	 * @param id
	 * @return
	 */
	@PostMapping(path = "/api/templates/delete")
	public GenericMessage deleteTemplate(@RequestParam(value = "id", required = true) String id) {
		return templateService.deleteTemplate(id);
	}
}
