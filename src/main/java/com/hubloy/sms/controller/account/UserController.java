package com.hubloy.sms.controller.account;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hubloy.sms.beans.account.AccountUserBean;
import com.hubloy.sms.beans.account.UserBean;
import com.hubloy.sms.orm.account.User;
import com.hubloy.sms.service.account.AccessService;

@RestController
public class UserController {
	
	@Autowired
	private AccessService accessService;
	
	@GetMapping(path = "/api/me")
	public UserBean getProfile(@AuthenticationPrincipal User user) {
		return accessService.getProfile(user);
	}
	
	@GetMapping(path = "/api/me/account")
	public AccountUserBean getUserAccount(@AuthenticationPrincipal User user) {
		return accessService.getUserAccount(user);
	}
}
