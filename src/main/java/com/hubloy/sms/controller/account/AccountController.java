package com.hubloy.sms.controller.account;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hubloy.sms.base.GenericMessage;
import com.hubloy.sms.beans.account.AccountBean;
import com.hubloy.sms.beans.account.AccountListBean;
import com.hubloy.sms.beans.account.AccountUserBean;
import com.hubloy.sms.beans.account.AccountUserListBean;
import com.hubloy.sms.beans.common.IntegerBean;
import com.hubloy.sms.orm.account.User;
import com.hubloy.sms.service.account.AccountService;

@RestController
public class AccountController {

	@Autowired
	private AccountService accountService;

	/**
	 * List Accounts
	 * 
	 * @param page
	 * @return
	 */
	@GetMapping(path = "/api/account/list")
	public AccountListBean listAccounts(@RequestParam(value = "page", required = true) int page) {
		return accountService.listAccounts(page);
	}

	/**
	 * List Account users
	 * 
	 * @param id
	 * @param page
	 * @return
	 */
	@GetMapping(path = "/api/account/users/list")
	public AccountUserListBean listAccountUsers(@RequestParam(value = "id", required = true) String id,
			@RequestParam(value = "page", required = true) int page) {
		return accountService.listAccountUsers(id, page);
	}

	/**
	 * Save Account user
	 * 
	 * @param accountName
	 * @param name
	 * @param email
	 * @param login
	 * @param password
	 * @return
	 */
	@PostMapping(path = "/api/account/save")
	public AccountBean saveAccount(@RequestParam(value = "account_name", required = true) String accountName,
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "email", required = true) String email,
			@RequestParam(value = "login", required = true) String login,
			@RequestParam(value = "password", required = true) String password) {
		return accountService.saveAccount(accountName, name, email, login, password);
	}

	/**
	 * Get Account
	 * 
	 * @param id
	 * @return
	 */
	@GetMapping(path = "/api/account")
	public AccountBean getAccount(@RequestParam(value = "id", required = true) String id) {
		return accountService.getAccount(id);
	}

	/**
	 * Update Account
	 * 
	 * @param id
	 * @param name
	 * @param credits
	 * @param status
	 * @return
	 */
	@PostMapping(path = "/api/account/update")
	public AccountBean updateAccount(@RequestParam(value = "id", required = true) String id,
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "credits", required = true) Long credits,
			@RequestParam(value = "status", required = true) String status) {
		return accountService.updateAccount(id, name, credits, status);
	}

	/**
	 * Toggle account status
	 * 
	 * @param id
	 * @return
	 */
	@PostMapping(path = "/api/account/toggle")
	public GenericMessage toggle(@RequestParam(value = "id", required = true) String id) {
		return accountService.toggle(id);
	}

	/**
	 * Save Account User
	 * 
	 * @param id
	 * @param name
	 * @param email
	 * @param login
	 * @param password
	 * @return
	 */
	@PostMapping(path = "/api/account/user/save")
	public GenericMessage saveAcountUser(@RequestParam(value = "id", required = true) String id,
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "email", required = true) String email,
			@RequestParam(value = "login", required = true) String login,
			@RequestParam(value = "password", required = true) String password) {
		return accountService.saveAcountUser(id, name, email, login, password);
	}

	/**
	 * Delete Account user
	 * 
	 * @param id
	 * @return
	 */
	@PostMapping(path = "/api/account/user/delete")
	public GenericMessage removeAccountUser(@RequestParam(value = "id", required = true) String id) {
		return accountService.removeAccountUser(id);
	}

	/**
	 * Get user
	 * 
	 * @param id
	 * @return
	 */
	@PostMapping(path = "/api/account/user/get")
	public AccountUserBean getAccountUser(@RequestParam(value = "id", required = true) String id) {
		return accountService.getAccountUser(id);
	}

	/**
	 * Update user
	 * 
	 * @param id
	 * @param name
	 * @param password
	 * @return
	 */
	@PostMapping(path = "/api/account/user/update")
	public GenericMessage updateAccountUser(@RequestParam(value = "id", required = true) String id,
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "password", required = false) String password) {
		return accountService.updateAccountUser(id, name, password);
	}

	/**
	 * Save Account credits
	 * 
	 * @param user
	 * @param id
	 * @param total
	 * @return
	 */
	@PostMapping(path = "/api/account/credits/save")
	public GenericMessage saveAccountCredits(@AuthenticationPrincipal User user,
			@RequestParam(value = "id", required = true) String id,
			@RequestParam(value = "total", required = true) Long total) {
		return accountService.saveAccountCredits(user, id, total);
	}

	/**
	 * Get Account credits
	 * 
	 * @param id
	 * @return
	 */
	@GetMapping(path = "/api/account/credits/{id}")
	public IntegerBean getAccountCredits(@PathVariable(value = "id") String id) {
		return accountService.getAccountCredits(id);
	}

	/**
	 * Reset password
	 * 
	 * @param username
	 * @return
	 */
	@PostMapping(path = "/data/account/reset")
	public GenericMessage resetPassword(@RequestParam(value = "username", required = true) String username) {
		return accountService.resetPassword(username);
	}
}
