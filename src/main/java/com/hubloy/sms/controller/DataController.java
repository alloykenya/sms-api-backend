package com.hubloy.sms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hubloy.sms.beans.common.DataBean;
import com.hubloy.sms.orm.account.User;
import com.hubloy.sms.service.DataService;

@RestController
public class DataController {

	@Autowired
	private DataService dataService;
	
	@GetMapping(path = "/api/data")
	public DataBean getData(@AuthenticationPrincipal User user) {
		return dataService.getData(user);
	}
}
