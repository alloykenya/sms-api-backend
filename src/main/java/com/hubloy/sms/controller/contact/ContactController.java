package com.hubloy.sms.controller.contact;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.hubloy.sms.base.GenericMessage;
import com.hubloy.sms.beans.contact.ContactListBean;
import com.hubloy.sms.beans.contact.GroupBean;
import com.hubloy.sms.beans.contact.GroupListBean;
import com.hubloy.sms.constants.ResponseStatus;
import com.hubloy.sms.orm.account.User;
import com.hubloy.sms.service.contact.ContactService;

@RestController
public class ContactController {

	@Autowired
	private ContactService contactService;

	@GetMapping(path = "/api/contacts/list")
	public ContactListBean listContacts(@AuthenticationPrincipal User user,
			@RequestParam(value = "page", required = true) int page) {
		return contactService.listContacts(user, page);
	}

	@GetMapping(path = "/api/contacts/group/list")
	public ContactListBean listGroupContacts(@AuthenticationPrincipal User user,
			@RequestParam(value = "group", required = true) String group,
			@RequestParam(value = "page", required = true) int page) {
		return contactService.listGroupContacts(user, group, page);
	}

	@GetMapping(path = "/api/contacts/groups")
	public GroupListBean listGroups(@AuthenticationPrincipal User user,
			@RequestParam(value = "page", required = true) int page) {
		return contactService.listGroups(user, page);
	}

	@GetMapping(path = "/api/contacts/groups/all")
	public List<GroupBean> listGroups(@AuthenticationPrincipal User user) {
		return contactService.listGroups(user);
	}

	@PostMapping(path = "/api/contacts/save")
	public GenericMessage saveContact(@AuthenticationPrincipal User user,
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "phone", required = true) String phone,
			@RequestParam(value = "group", required = false) String group) {
		return contactService.saveContact(user, name, phone, group);
	}

	@PostMapping(path = "/api/contacts/group/save")
	public GenericMessage saveGroup(@AuthenticationPrincipal User user,
			@RequestParam(value = "name", required = true) String name) {
		return contactService.saveGroup(user, name);
	}

	@PostMapping(path = "/api/contacts/delete")
	public GenericMessage deleteContact(@RequestParam(value = "id", required = true) String id) {
		return contactService.deleteContact(id);
	}

	@PostMapping(path = "/api/contacts/group/delete")
	public GenericMessage deleteContactGroup(@RequestParam(value = "id", required = true) String id) {
		return contactService.deleteContactGroup(id);
	}

	@GetMapping(path = "/api/contacts/group/get")
	public GroupBean getGroup(@RequestParam(value = "id", required = true) String id) {
		return contactService.getGroup(id);
	}

	@PostMapping(path = "/api/contacts/upload/csv")
	public GenericMessage uploadCSVContacts(@RequestParam(value = "group", required = false) String group,
			@AuthenticationPrincipal User user, @RequestParam("file") MultipartFile file) {
		CompletableFuture<GenericMessage> response = contactService.uploadCSVContacts(group, user, file);
		CompletableFuture.allOf(response).join();
		try {
			return response.get();
		} catch (InterruptedException e) {
			return new GenericMessage(ResponseStatus.ERROR, "An error occured " + e.getMessage());
		} catch (ExecutionException e) {
			return new GenericMessage(ResponseStatus.ERROR, "An error occured " + e.getMessage());
		}
	}
	
	
	@PostMapping(path = "/api/contacts/upload/xls")
	public GenericMessage uploadExcelContacts(@RequestParam(value = "group", required = false) String group,
			@AuthenticationPrincipal User user, @RequestParam("file") MultipartFile file) {
		CompletableFuture<GenericMessage> response = contactService.uploadExcelContacts(group, user, file);
		CompletableFuture.allOf(response).join();
		try {
			return response.get();
		} catch (InterruptedException e) {
			return new GenericMessage(ResponseStatus.ERROR, "An error occured " + e.getMessage());
		} catch (ExecutionException e) {
			return new GenericMessage(ResponseStatus.ERROR, "An error occured " + e.getMessage());
		}
	}
}
