package com.hubloy.sms;

import java.time.ZoneId;
import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmsApiBridgeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmsApiBridgeApplication.class, args);
	}
	
	@PostConstruct
	public void started() {
	    TimeZone.setDefault(TimeZone.getTimeZone(ZoneId.of("Africa/Blantyre")));
	}
}