package com.hubloy.sms.constants;

/**
 * 
 * Common Response Status
 * 
 * @author Paul Kevin
 *
 * @version enter version, 26 Feb 2018
 *
 * @since  jdk 1.8
 */
public enum ResponseStatus {
	SUCCESS, FAILURE, ERROR, UNAUTHORIZED, EXPIRED
}
