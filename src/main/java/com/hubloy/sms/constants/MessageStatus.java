package com.hubloy.sms.constants;

public enum MessageStatus {
	Pending, Processing, Sent, Error
}