package com.hubloy.sms.constants;

public enum FileStatus {
	Pending, Processing, Done, Error
}