package com.hubloy.sms.constants;

public enum AccountStatus {
	Active, OnHold, Expired, Deactivated
}