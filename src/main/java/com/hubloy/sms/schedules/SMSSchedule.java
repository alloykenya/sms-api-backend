package com.hubloy.sms.schedules;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.hubloy.sms.service.message.SMSService;

@Component
public class SMSSchedule {

	@Autowired
	private SMSService smsService;
	
	@Scheduled(cron="0 0/10 * * * *")
	public void processMessages() {
		smsService.process();
	}
}
