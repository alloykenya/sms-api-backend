package com.hubloy.sms.schedules;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.hubloy.sms.service.settings.SMSAccountService;

@Component
public class SettingsSchedule {

	@Autowired
	private SMSAccountService smsAccountService;
	
	@Scheduled(cron="0 0/5 * * * *")
	public void refreshCredits() {
		smsAccountService.refreshBalance();
	}
}