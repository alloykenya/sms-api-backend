/**
 *
 */
package com.hubloy.sms.repository.oauth;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hubloy.sms.orm.oauth.ClientDetails;

public interface ClientDetailsRepository extends JpaRepository<ClientDetails, String> {

	ClientDetails findByClientId(String client_id);
}