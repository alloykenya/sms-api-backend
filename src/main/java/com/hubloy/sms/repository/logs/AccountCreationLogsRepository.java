package com.hubloy.sms.repository.logs;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hubloy.sms.orm.logs.AccountCreationLogs;

public interface AccountCreationLogsRepository extends JpaRepository<AccountCreationLogs, Long>{

}
