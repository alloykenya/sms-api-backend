package com.hubloy.sms.repository.logs;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hubloy.sms.orm.logs.MessageLog;

public interface MessageLogRepository extends JpaRepository<MessageLog, Long>{

}
