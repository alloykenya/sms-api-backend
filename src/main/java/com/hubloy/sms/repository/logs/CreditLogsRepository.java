package com.hubloy.sms.repository.logs;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hubloy.sms.orm.logs.CreditLogs;

public interface CreditLogsRepository extends JpaRepository<CreditLogs, Long>{

}
