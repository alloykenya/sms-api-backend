package com.hubloy.sms.repository.account;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.hubloy.sms.orm.account.User;

public interface UserRepository extends JpaRepository<User, Long> {

	User findByLogin(String login);
	
	User findByItemId(String itemid);
	
	Long countByActive(boolean active);
	
	List<User> findByActive(boolean active, Pageable pageable);
}
