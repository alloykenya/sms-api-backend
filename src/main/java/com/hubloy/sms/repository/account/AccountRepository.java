package com.hubloy.sms.repository.account;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.hubloy.sms.constants.AccountStatus;
import com.hubloy.sms.orm.account.Account;
import com.hubloy.sms.orm.account.User;

public interface AccountRepository extends JpaRepository<Account, Long>{
	
	Account findByAdmin(User user);
		
	Account findByItemId(String itemid);
	
	@Query("SELECT a FROM Account a WHERE a.accountType = 'System'")
	Account getSystemAccout();
	
	List<Account> findByAccountStatus(AccountStatus accountStatus, Pageable pageable);
	
	Long countByAccountStatus(AccountStatus accountStatus);
	
	@Query("SELECT SUM(a.credits) FROM Account a WHERE a.accountType = 'User'")
	Long getTotalCredit();
}