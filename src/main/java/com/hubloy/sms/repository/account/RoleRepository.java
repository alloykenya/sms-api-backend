package com.hubloy.sms.repository.account;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hubloy.sms.orm.account.Role;

public interface RoleRepository extends JpaRepository<Role, Long>{
	
	Role findByName(String name);
}
