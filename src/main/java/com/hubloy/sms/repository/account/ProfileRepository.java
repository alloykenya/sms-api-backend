package com.hubloy.sms.repository.account;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hubloy.sms.orm.account.Profile;
import com.hubloy.sms.orm.account.User;

public interface ProfileRepository extends JpaRepository<Profile, Long> {

	Profile findByEmail(String email);

	Profile findByPhone(String phone);

	Profile findByUser(User user);
}
