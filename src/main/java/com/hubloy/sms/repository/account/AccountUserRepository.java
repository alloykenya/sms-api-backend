package com.hubloy.sms.repository.account;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.hubloy.sms.orm.account.Account;
import com.hubloy.sms.orm.account.AccountUser;
import com.hubloy.sms.orm.account.User;

public interface AccountUserRepository extends JpaRepository<AccountUser, Long>{
	
	AccountUser findByItemId(String itemid);
	
	List<AccountUser> findByAccount(Account account);
	
	AccountUser findByUser(User user);
	
	AccountUser findByUserAndAccount(User user,Account account);
	
	List<AccountUser> findByAccount(Account account, Pageable pageable);
	
	Long countByAccount(Account account);
}
