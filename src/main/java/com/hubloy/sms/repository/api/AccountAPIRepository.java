package com.hubloy.sms.repository.api;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hubloy.sms.orm.api.AccountAPI;

public interface AccountAPIRepository extends JpaRepository<AccountAPI, Long> {
	
	AccountAPI findByApiKey(String apiKey);
	
	AccountAPI findByItemId(String itemId);
}
