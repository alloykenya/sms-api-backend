package com.hubloy.sms.repository.contacts;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.hubloy.sms.orm.account.Account;
import com.hubloy.sms.orm.contacts.Contact;
import com.hubloy.sms.orm.contacts.ContactGroup;

public interface ContactRespository extends JpaRepository<Contact, Long> {

	Contact findByItemId(String itemid);

	List<Contact> findByAccount(Account account, Pageable pageable);

	Long countByAccount(Account account);
	
	Long countByContactGroup(ContactGroup contactGroup);

	List<Contact> findByAccountAndContactGroup(Account account, ContactGroup contactGroup, Pageable pageable);

	Long countByAccountAndContactGroup(Account account, ContactGroup contactGroup);
	
	List<Contact> findByContactGroup(ContactGroup contactGroup, Pageable pageable);
	
	List<Contact> findByContactGroup(ContactGroup contactGroup);
}
