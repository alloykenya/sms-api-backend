package com.hubloy.sms.repository.contacts;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.hubloy.sms.orm.account.Account;
import com.hubloy.sms.orm.contacts.ContactGroup;

public interface ContactGroupRepository extends JpaRepository<ContactGroup, Long>{
	
	ContactGroup findByItemId(String itemId);
	
	List<ContactGroup> findByAccount(Account account, Pageable pageable);
	
	Long countByAccount(Account account);
	
	List<ContactGroup> findByAccount(Account account);
}
