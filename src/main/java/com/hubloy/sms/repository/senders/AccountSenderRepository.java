package com.hubloy.sms.repository.senders;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.hubloy.sms.orm.account.Account;
import com.hubloy.sms.orm.senders.AccountSender;
import com.hubloy.sms.orm.senders.SMSSender;

public interface AccountSenderRepository extends JpaRepository<AccountSender, Long> {

	List<AccountSender> findByAccount(Account account, Pageable pageable);
	
	List<AccountSender> findByAccount(Account account);

	Long countByAccount(Account account);

	AccountSender findByItemId(String itemId);

	AccountSender findByItemIdAndAccount(String itemId, Account account);
	
	AccountSender findBySmsSenderAndAccount(SMSSender smsSender, Account account);
	
	List<AccountSender> findBySmsSender(SMSSender smsSender);
	
	Long countBySmsSender(SMSSender smsSender);
}
