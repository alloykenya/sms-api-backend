package com.hubloy.sms.repository.senders;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.hubloy.sms.orm.senders.SMSSender;
import com.hubloy.sms.orm.settings.SMSAPISettings;

public interface SMSSenderRepository extends JpaRepository<SMSSender, Long>{
	
	List<SMSSender> findBySettings(SMSAPISettings smsapiSettings, Pageable pageable);
	
	Long countBySettings(SMSAPISettings smsapiSettings);
	
	SMSSender findByItemId(String itemId);
	
	SMSSender findBySender(String sender);
}
