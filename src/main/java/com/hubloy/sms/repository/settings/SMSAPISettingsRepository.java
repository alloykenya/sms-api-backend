package com.hubloy.sms.repository.settings;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hubloy.sms.orm.settings.SMSAPISettings;

public interface SMSAPISettingsRepository extends JpaRepository<SMSAPISettings, Long>{

	SMSAPISettings findByItemId(String itemId);
}
