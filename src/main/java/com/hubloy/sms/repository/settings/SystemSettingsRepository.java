package com.hubloy.sms.repository.settings;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hubloy.sms.orm.settings.SystemSettings;

public interface SystemSettingsRepository extends JpaRepository<SystemSettings, Long> {
	SystemSettings findByItemId(String itemId);

	SystemSettings findBySetting(String setting);
}