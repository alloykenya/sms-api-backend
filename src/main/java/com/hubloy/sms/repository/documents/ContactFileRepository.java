package com.hubloy.sms.repository.documents;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hubloy.sms.orm.documents.ContactFile;

public interface ContactFileRepository extends JpaRepository<ContactFile, Long>{

}
