package com.hubloy.sms.repository.documents;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hubloy.sms.orm.documents.MessageFile;

public interface MessageFileRepository extends JpaRepository<MessageFile, Long>{

}
