package com.hubloy.sms.repository.message;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.hubloy.sms.orm.account.Account;
import com.hubloy.sms.orm.message.MessageTemplate;

public interface MessageTemplateRepository extends JpaRepository<MessageTemplate, Long>{
	
	List<MessageTemplate> findByAccount(Account account, Pageable pageable);
	
	List<MessageTemplate> findByAccount(Account account);
	
	MessageTemplate findByItemId(String itemId);
	
	Long countByAccount(Account account);
}
