package com.hubloy.sms.repository.message;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.hubloy.sms.constants.MessageStatus;
import com.hubloy.sms.orm.account.Account;
import com.hubloy.sms.orm.message.MessageToSend;

public interface MessageToSendRepository extends JpaRepository<MessageToSend, Long> {

	Long countByAccountAndMessageStatus(Account account, MessageStatus messageStatus);

	Long countByAccount(Account account);

	List<MessageToSend> findByAccount(Account account, Pageable pageable);

	@Query("SELECT t FROM MessageToSend t WHERE t.messageStatus = :status AND t.schedule <= now()")
	List<MessageToSend> findByMessageStatusAndSchedule(@Param("status") MessageStatus messageStatus, Pageable pageable);

	@Query("SELECT t FROM MessageToSend t WHERE t.account = :account AND t.dateCreated BETWEEN :startDate AND :endDate")
	List<MessageToSend> findByAccountAndDate(@Param("account") Account account, @Param("startDate") Date startDate,
			@Param("endDate") Date endDate);

	@Query("SELECT t FROM MessageToSend t WHERE t.account = :account AND t.dateCreated <= :endDate")
	List<MessageToSend> findByAccountAndEndDate(@Param("account") Account account, @Param("endDate") Date endDate);
	
	@Query("SELECT t FROM MessageToSend t WHERE t.account = :account AND t.dateCreated >= :startDate")
	List<MessageToSend> findByAccountAndStartDate(@Param("account") Account account, @Param("startDate") Date startDate);
}